# Changelog

Alle nennenswerten Änderungen an diesem Projekt werden in dieser Datei dokumentiert.

Das Format basiert auf [Führe ein CHANGELOG](https://keepachangelog.com/de/).

## [unreleased]

### Grundregelwerk

#### Add

- Schwierigkeitsgrade

#### Changed

- Erfolge bereits ab eriner gewürfelten 8
- Attribute, Fertigkeiten und Kompetenzen
- Initiative Reaktions-Probe

#### Removed

- Musterklassen
- Waffen und Rüstungen

### Magieregelwerk

#### Changed

- Zauberstärktekosten erhöht

#### Removed

- Kampfmagie

### Stuntbeispiele

#### Changed

- Kategorie umbenannt und Waffen und Rüstungen hinzugefügt

## [2024-09-21]

### Grundregelwerk

#### Add

- Rüstungsschaden
- Kampfmanöver Burst
- Waffen aus Zeitepochen
- Zielen
- Kritische Treffer erzeugen Schaden
- Fehschlag beim Fernkampfanriff

#### Changed

- Gezielter Angriff
- Kritischer Treffer
- Keine kritische Misserfolge bei Rettungswüfen
- Schaden vor Rüstungsabzug
- Parieren als konkurrierende Probe
- Aktionen im Konflikt
- Rückzug
- Schicksalpunkte durch kritische Treffer
- Konsequenzen regeneration

### Magieregelwerk

#### Add

- Magiewerteberechnung

#### Changed

- Wirkungsbereich
- Zauberbeispiele

#### Removed

- Reichweite 0

## [2024-05-29]

### Grundregelwerk

#### Add

- Gelgenheitsangriff Fernkampf ([#25](https://codeberg.org/Moirenico/mdBook/issues/25))
- Stabilisieren
- Kritischer Schaden ([#28](https://codeberg.org/Moirenico/mdBook/issues/28))
- Rettungswürfe ([#27](https://codeberg.org/Moirenico/mdBook/issues/27#issuecomment-1530418))
- Traumata
- Aspektbeispiele
- Kompetenzen

#### Changed

- Bewegungsaktion
- Vom Kampf lösen ([#26](https://codeberg.org/Moirenico/mdBook/issues/26))
- Fernkampfwaffen umbenannt
- Implementierung neuer Fertigkeiten und Attribute ([#27](https://codeberg.org/Moirenico/mdBook/issues/27))
- Konkurrierende Proben
- Materialien Ausrüstung

#### Removed

- Kampf mit zwei Waffen
- Mehrfachangriff
- Aspekt-Kompetenzbeispiele

### Magieregelwerk

#### Add

- Zaubermeisterschaft
- Weitere Zufallseffekte
- Zauberpunkte für besondere Effekte

#### Changed

- Wort Zeit
- Rework wegen den neuen Fertigkeiten und Attribute ([#27](https://codeberg.org/Moirenico/mdBook/issues/27))

### Konstruktionsregelwerk

#### Changed

- Rework wegen den neuen Fertigkeiten und Attribute ([#27](https://codeberg.org/Moirenico/mdBook/issues/27))

### Stuntbeispiele

#### Removed

- Gefährten

## Release [2023-10-29]

### Grundregelwerk

#### Add

- Variabler Kampfstil für Schwert und Schild

#### Changed

- Gelegenheitsangriff
- Vom Kampf lösen

#### Removed

- Abgebrannt und Geldsegen

### Magieregelwerk

#### Changed

- Schaden & Widerstand Feuerball

## Release [2023-09-27]

### Grundregelwerk

#### Add

- Boni für Nahkampffertigkeiten
- Boni für Matrial Holz

#### Changed

- Beschreibung des Würfelmechanismusses überarbeitet
- Schadenswürfe verändert
- Nahkampffertigkeiten
- Umbenennung Schwere Waffen

### Magieregelwerk

#### Changed

- Silben feiner unterteilt, es gibt jetzt 10 pro Wort
- Schadenswürfe verändert

#### Removed

- Schwierigkeit der Zauber
- Magiekategorie Silben

## Release [2023-09-19]

### Magieregelwerk

#### Changed

- Worte in ein Dokument zusammengefasst

## Release [2023-09-18]

### Grundregelwerk

#### Changed

- Beschreibung und Nachteil bei Equipment
- Erfahrungsverteilung

### Magieregelwerk

#### Changed

- Kategorie Worte in Zaubern umbenannt

## Release [2023-07-31]

### Grundregelwerk

#### Add

- Kauf von Spezialaktionen mit Aspekten
- Beispiel für einen Schub
- Materialien für Waffen und Rüstungen ([#22](https://codeberg.org/Moirenico/mdBook/issues/22))

#### Changed

- Spezialaktionen bei Charaktererstellung reduziert
- Aspekte bei Charaktererstellung reduziert
- Eigene Kategorie für Ausürstung
- Restukturierung Aktionen und Kampf

#### Removed

- Doppelte Kategorie Spezialaktionen in der Charaktererstellung

### Magieregelwerk

#### Changed

- Trennung Zauber und Gegenstandsbeispiele
- Zauberrückgewinnung nach Schlafen

## Release [2023-06-12]

### Grundregelwerk

#### Add

- Erwerb Spezialaktionen

#### Changed

- Herstellungszeit von Technik und Alchemie aufgeteilt
- Attrubite werden, bei der Charaktererschaffung, nicht mehr gekauft sondern getauscht
- Ressourcenwert feiner aufgeteilt ([#11](https://codeberg.org/Moirenico/mdBook/issues/11))

#### Removed

- Erfolgsgrad "sehr einfach" ([#19](https://codeberg.org/Moirenico/mdBook/pulls/19))
- Stuntbeispiele entfernt
- Attributspunktekauf mit Schwächen
- Werte tauschen

### Magieregelwerk

#### Changed

- Elementarmagie aufgeteilt
- 2 Magiekategorien für einen Stunt

## Release [2023-06-03]

### Grundregelwerk

#### Add

- Kauf von Spezialaktionen bei Charaktererstellung
- Flächenangriffe
- Volle Verteidigung

#### Changed

- Schwächen sind Aspekte
- Maximale Stunts angegeben
- Waffenlos in Unbewaffnet umbenannt

### Magieregelwerk

#### Add

- Regeneration Zauberanzahl
- Beispielzauber Segnung
- Reset von Zauberanzahl
- Zufallseffekt 32

#### Removed

- Beschreibung beim Wort "Element"

### Stuntbeispiele

#### Changed

- Waffenlos in Unbewaffnet umbenannt

## Release [2023-02-05]

### Grundregelwerk

#### Add

- Intelligenzfertigkeit Knobeln
- mehr Pflichtfertigkeiten

#### Remove

- Spezialaktionen

### Magieregelwerk

#### Add

- Nutzung von Zaubern erhöht die Schwierigkeit

## Release [2023-02-01]

### Grundregelwerk

#### Add

- Wertetausch
- Schwächen
- Körperwerte ([#18](https://codeberg.org/Moirenico/mdBook/pulls/18))
- Anzahl Spezialaktionen ([#18](https://codeberg.org/Moirenico/mdBook/pulls/18))
- Fernkampffertigkeiten Werfen ([#18](https://codeberg.org/Moirenico/mdBook/pulls/18))

#### Changed

- Charaktererstellungstabelle ergänzt
- Geisteswissenschaften beinhaltet Kunst
- Schicksalspunkte sind Spielergebunden
- Fähigkeitspunkte bei Charaktererschaffung
- Lebensberechnung

### Magieregelwerk

#### Changed

- Aufrechterhaltende Zauber besser formuliert
- Magiearten

### Konstruktionsregelwerk

#### Changed

- Vorteile eingefügt

## Release [2022-11-21]

### Grundregelwerk

#### Changed

- Zufallseffekt Größe auf *5 verringert
- Nahkampf mit Nahkampffähigkeit Parieren ([#15](https://codeberg.org/Moirenico/mdBook/pulls/15))
- Ausweichen im Nahkampf mit Malus ([#15](https://codeberg.org/Moirenico/mdBook/pulls/15))
- Vor- und Nachteil eingeführt ([#13](https://codeberg.org/Moirenico/mdBook/issues/13)) ([#16](https://codeberg.org/Moirenico/mdBook/pulls/16))
- Kauf von Spezialaktionen
- Aspekte können Patzer abwenden ([#9](https://codeberg.org/Moirenico/mdBook/issues/9))

## Release [2022-10-29]

### Grundregelwerk

#### Add

- Ressourcenkosten für Waffen und Rüstungen
- Handhabung mehrere Schübe
- Spezialaktion
- Kampfskills ([#12](https://codeberg.org/Moirenico/mdBook/pulls/12))

#### Changed

- Bezeichnung eines Ressourcenpunkts
- Orte & Personen zu Geschichten zusammengefasst
- Beispielklassen angepasst ([#8](https://codeberg.org/Moirenico/mdBook/issues/8))

#### Remove

- Gegenstandstufe der magischen Gegenstände ([#7](https://codeberg.org/Moirenico/mdBook/issues/7))
- Rüstungsmalus

### Magieregelwerk

#### Add

- Beispielzauber Taschendimension

#### Changed

- Zusatzeffekte Rattenschwarm hinzugefügt ([#6](https://codeberg.org/Moirenico/mdBook/issues/6))
- Text Feuerball
- Zeiteinteilung für Kampfzauber
- Berechnung der Spruchliste

## Release [2022-08-28]

### Grundregelwerk

#### Add

- Schilde

#### Changed

- Athletik als Pflichtfertigkeit
- Rundung bei Patzern
- Modifikatoren einheitlich benannt

#### Remove

- Blocken und Ausweichen

### Magieregelwerk

#### Changed

- Reinigung genauer erklärt

## Release [2022-08-15]

### Grundregelwerk

#### Add

- Schwierigkeitstabelle um Heilungsmodifikator ergänzt
- Pflichtfertigkeiten

#### Changed

- Waffen und Rüstungen
- Kampf- und Schadensmodell

#### Remove

- Explosion der Bonuswürfel

### Magieregelwerk

#### Changed

- Schadensmodell

## Release [2022-04-09]

### Grundregelwerk

#### Add

- Bonuswürfel ([#3](https://codeberg.org/Moirenico/mdBook/issues/3))

#### Changed

- Sortierung Zeiteinteilung

### Magieregelwerk

#### Changed

- Zufallseffekttabelle verbessert
- Patzertabelle verbessert

## Release [2022-02-25]

Ich habe die Auslieferung des Regelwerks einmal grundlegend geändert, da es noch öfters Änderungen geben wird habe ich mich für ein Webbasiertes ausliefern entschieden. Sobald das Regelwerk einen stabilen stand erreicht hat, werde ich mich wieder um eine PDF Version kümmern.

### Grundregelwerk

#### Add

- Kampfmanöver
- Reaktion
- Statuseffekte
- Erkärung Schub(Aspekt)
- Pseudo Ressourcenmonitor
- neue Seite "Aspekte aufmotzen"

#### Changed

- typo Akrobatik entfernt
- Beschreibung Würfeln verbessert
- Blocken/Ausweichen als Reaktion

#### Remove

- Bezeichnung Gamemaster
- Waffenwerte und Tabellen
- Ressourcen und Ansehen

### Magieregelwerk

#### Add

- Benutzung von Aspekten
- zweite Zauberart

#### Changed

- typo Akrobatik entfernt
- Heilung der dritten Silbe korrigiert
- Zufallseffekte abgemildert und angepasst

#### Remove

- Erkärung Schub(Aspekt)

### Stuntbeispiele

#### Changed

- Sechster Sinn
- Sprachbegabt

### Preisliste

#### Remove

- Gesamte Liste entfernt

[unreleased]: https://codeberg.org/Moirenico/mdBook/compare/2024-09-21...develop
[2024-09-21]: https://codeberg.org/Moirenico/mdBook/compare/2024-05-29...2024-09-21
[2024-05-29]: https://codeberg.org/Moirenico/mdBook/compare/2023-10-29...2024-05-29
[2023-10-29]: https://codeberg.org/Moirenico/mdBook/compare/2023-09-27...2023-10-29
[2023-09-27]: https://codeberg.org/Moirenico/mdBook/compare/2023-09-19...2023-09-27
[2023-09-19]: https://codeberg.org/Moirenico/mdBook/compare/2023-09-18...2023-09-19
[2023-09-18]: https://codeberg.org/Moirenico/mdBook/compare/2023-07-31...2023-09-18
[2023-07-31]: https://codeberg.org/Moirenico/mdBook/compare/2023-06-12...2023-07-31
[2023-06-12]: https://codeberg.org/Moirenico/mdBook/compare/2023-06-03...2023-06-12
[2023-06-03]: https://codeberg.org/Moirenico/mdBook/compare/2023-02-05...2023-06-03
[2023-02-05]: https://codeberg.org/Moirenico/mdBook/compare/2023-02-01...2023-02-05
[2023-02-01]: https://codeberg.org/Moirenico/mdBook/compare/2022-11-21...2023-02-01
[2022-11-21]: https://codeberg.org/Moirenico/mdBook/compare/2022-08-28...2022-11-21
[2022-10-29]: https://codeberg.org/Moirenico/mdBook/compare/2022-08-28...2022-10-29
[2022-08-28]: https://codeberg.org/Moirenico/mdBook/compare/2022-08-15...2022-08-28
[2022-08-15]: https://codeberg.org/Moirenico/mdBook/compare/2022-04-09...2022-08-15
[2022-04-09]: https://codeberg.org/Moirenico/mdBook/compare/2022-02-25...2022-04-09
[2022-02-25]: https://codeberg.org/Moirenico/mdBook/src/tag/2022-02-25
