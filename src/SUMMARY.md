# Summary

[Einleitung](./einleitung.md)
[Changelog](./CHANGELOG.md)

---

# Grundregelwerk

- [Allgemein](./grundregelwerk/index.md)
- [Charaktererstellung](./grundregelwerk/charaktererstellung/index.md)
- [Charakterentwicklung](./grundregelwerk/charakterentwicklung/index.md)
- [Ressourcen](./grundregelwerk/ressourcen/index.md)
- [Attribute & Fertigkeiten](./grundregelwerk/attribute_fertigkeiten/index.md)
- [Konflikte](./grundregelwerk/konflikte/index.md)
  - [Kampf](./grundregelwerk/konflikte/kampf.md)
  - [Verletzungen](./grundregelwerk/konflikte/verletzungen.md)
- [Kompetenzen](./grundregelwerk/kompetenzen/index.md)
  - [Nahkampf](./grundregelwerk/kompetenzen/nahkampf.md)
  - [Fernkampf](./grundregelwerk/kompetenzen/fernkampf.md)
  - [Fachkenntnisse](./grundregelwerk/kompetenzen/fachkenntnisse.md)
  - [Diplomatie](./grundregelwerk/kompetenzen/diplomatie.md)
- [Schicksalspunkte](./grundregelwerk/schicksalspunkte/index.md)
- [Aspekte](./grundregelwerk/aspekte/index.md)
  - [Ein guter Aspekt](./grundregelwerk/aspekte/guter_aspekt.md)
  - [Aspekte Aufmotzen](./grundregelwerk/aspekte/aspekte_aufmotzen.md)
  - [Schub](./grundregelwerk/aspekte/schub.md)
  - [Schwächen](./grundregelwerk/aspekte/schwaechen.md)
  - [Beispiele](./grundregelwerk/aspekte/beispiele.md)
- [Stunts](./grundregelwerk/stunts/index.md)
  - [Stuntschablonen](./grundregelwerk/stunts/stuntschablonen.md)
  - [Stunts anpassen](./grundregelwerk/stunts/stunts_anpassen.md)
- [Zeiteinteilung](./grundregelwerk/zeiteinteilung/index.md)
  - [Reisen](./grundregelwerk/zeiteinteilung/reisen.md)
  - [Lager](./grundregelwerk/zeiteinteilung/lager.md)
  - [Zufallstabelle](./grundregelwerk/zeiteinteilung/zufallstabelle.md)

---

# Magieregelwerk

- [Allgemein](./magieregelwerk/index.md)
- [Magie](./magieregelwerk/magie/index.md)
- [Zaubern](./magieregelwerk/zaubern/index.md)
  - [Wirken](./magieregelwerk/zaubern/wirken.md)
- [Magiekategorien](./magieregelwerk/magiekategorien/index.md)
- [Gegenstände](./magieregelwerk/gegenstaende/index.md)
- [Zauberbeispiele](./magieregelwerk/zauberbeispiele/index.md)
- [Zusatzeffekte](./magieregelwerk/zusatzeffekte/index.md)
  - [Patzertabelle](./magieregelwerk/zusatzeffekte/patzertabelle.md)

---

# Konstruktionsregelwerk

- [Allgemein](./konstruktionsregelwerk/index.md)
- [Technik](./konstruktionsregelwerk/technik/index.md)
- [Alchemie](./konstruktionsregelwerk/alchemie/index.md)

---

# Material

- [Waffen](./material/waffen/index.md)
  - [Verbesserungen](./material/waffen/verbesserung.md)
- [Rüstungen](./material/ruestungen/index.md)
  - [Verbesserungen](./material/ruestungen/verbesserung.md)
- [Stuntsbeispiele](./material/stuntbeispiele/index.md)

---

[Impressum](./imprint.md)
[Datenschutzhinweise](./privacy.md)
