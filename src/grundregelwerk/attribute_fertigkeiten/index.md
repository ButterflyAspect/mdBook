# Attribute & Fertigkeiten

## Attribute

**Körperbau:** \
Berechnung [Wunden](/grundregelwerk/konflikte/verletzungen.md#wunden--stress) \
Dies repräsentiert die körperliche Stärke, Belastbarkeit und die Fähigkeit, anstrengende Aktivitäten über einen längeren Zeitraum aufrechtzuerhalten. Ein hoher Wert in Stärke wird auch für den Umgang mit starken Nahkampfwaffen benötigt.

**Präzision:** \
Dieses Attribut spiegelt die Fähigkeit des Charakters wider, Bewegungen präzise zu kontrollieren und geschickt mit Werkzeugen oder Fernkampfwaffen umzugehen.

**Logik:** \
Dieses Attribut steht für Wissen und intellektuelle Fähigkeiten. Es wird auch für Spezialwissen in Bereichen wie Technik, Handwerk, Wissenschaft und Medizin benötigt.

**Empathie:** \
Berechnung [Stress](/grundregelwerk/konflikte/verletzungen.md#wunden--stress) \
Die Fähigkeit zur Empathie beeinflusst die sozialen Fähigkeiten und ermöglicht den Umgang mit Tieren.

## Fertigkeiten

Die Fertigkeiten sind den standardmäßig zugewiesenen Attributen zugeordnet, können aber je nach Anwendungsfall variieren.

**Nahkampf (Körperbau)** \
Repräsentiert deine Fähigkeiten im Umgang mit Nahkampfwaffen und deine Fähigkeit, Nahkampfangriffe zu parieren. \
Mit welchen Waffen du trainiert bist, kannst du über eine [Kompetenz](/grundregelwerk/kompetenzen/index.md) festlegen.

**Kraft (Körperbau)** \
Körperkraft ist wichtig, um körperliche Herausforderungen zu meistern und in verschiedenen Umgebungen zu überleben.

**Zähigkeit (Körperbau)** \
Dieser [Rettungswurf](/grundregelwerk/index.md#rettungsw%C3%BCrfe) misst die körperliche Widerstandskraft und die Fähigkeit des Charakters, sich gegen physische Bedrohungen durch Gift, Krankheit oder extreme Umweltbedingungen zu wehren.

**Fernkampf (Präzision)** \
Repräsentiert deine Fähigkeiten im Umgang mit Fernkampfwaffen. \
Mit welchen Waffen du trainiert bist, kannst du über eine [Kompetenz](/grundregelwerk/kompetenzen/index.md) festlegen.

**Bewegung (Präzision)** \
Diese Fertigkeit ermöglicht es dem Charakter, sich geschickt und wendig durch verschiedene Umgebungen zu bewegen. Dazu gehören Klettern, Laufen, Reiten und Fahren.

**List (Präzision)** \
Gerissene Charaktere sind Meister der Täuschung und des unbemerkten Handelns. Dazu gehören Schleichen, Taschendiebstahl und das Verstecken vor Feinden.

**Reaktion (Präzision)** \
Der [Rettungswurf](/grundregelwerk/index.md#rettungsw%C3%BCrfe) stellt die Fähigkeit des Charakters dar, schnellen oder überraschenden Bedrohungen auszuweichen. Dies können Fallen, plötzliche Angriffe oder andere Situationen sein, in denen schnelle Reflexe entscheidend sind.

**Fachkenntnisse (Logik)** \
Charaktere mit umfassendem Wissen können komplexe Technologien reparieren, fortgeschrittenes Handwerk ausüben und wissenschaftliche Probleme lösen. \
Welche Fachkentnisse du besitzt, kannst du über eine [Kompetenz](/grundregelwerk/kompetenzen/index.md) festlegen.

**Mysterien (Logik)** \
Die Fähigkeit, mit Geheimnissen umzugehen, hilft dem Charakter, Rätsel zu lösen, übernatürliche Phänomene zu verstehen und verborgene Geheimnisse zu entschlüsseln.

**Ortskenntnis (Logik)** \
Ein Charakter mit hoher Wahrnehmung ist in der Lage, subtile Hinweise und Gefahren zu erkennen, Unsichtbares aufzudecken und aufmerksam in seiner Umgebung zu agieren.

**Wahrnehmung (Logik/Empathie)** \
Ein Charakter mit hoher Wahrnehmung ist in der Lage, subtile Hinweise und Gefahren zu erkennen, Unsichtbares aufzudecken und aufmerksam in seiner Umgebung zu agieren.

**Diplomatie (Empathie)** \
Charismatische Charaktere können erfolgreich verhandeln, andere einschätzen, täuschen und überzeugen. Dies ist besonders wichtig in sozialen Interaktionen und politischen Situationen. \
Welche soziale Fähigkeiten du besitzt, kannst du über eine [Kompetenz](/grundregelwerk/kompetenzen/index.md) festlegen.

**Umgang mit Tieren (Empathie)** \
Dieser Skill ermöglicht es dem Charakter, mit Tieren zu kommunizieren (soweit möglich), sie zu beruhigen, zu kontrollieren und zu reiten.

**Willenskraft (Empathie)** \
Dieser [Rettungswurf](/grundregelwerk/index.md#rettungsw%C3%BCrfe) bewertet die mentale Stärke des Charakters und seine Fähigkeit, sich gegen mentale Einflüsse wie Zaubersprüche, Illusionen oder mentale Manipulation zu wehren.
