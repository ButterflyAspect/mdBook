# Lager errichten

Nur ein Gruppenmitglied kann sich um den Lagerplatz kümmern. Das kann **nicht** mit Wandern kombiniert werden.

Mit einer normalen Fachkenntnisse-Probe kann ein guter Platz für ein Lager gefunden werden, ein Feuer entfacht und ein Platz zum Schlafen vorbereitet werden. Die anderen Gruppenmitglieder können beim Errichten des Lagers helfen. Für jeden Helfenden gibt es einen Vorteil. Falls die Probe misslingt, würfelt auf die [Misserfolg Lager Tabelle](/grundregelwerk/zeiteinteilung/zufallstabelle.md#misserfolg-lager).

## Rasten

Kann **nicht** mit Wandern kombiniert werden.

Ausruhen dauert eine Tageseinheit. Wenn das Ausruhen nicht unterbrochen wird, startet der natürliche Heilungsprozess.

## Schlafen

Kann **nicht** mit Wandern kombiniert werden.

Falls der Charakter nicht mindestens eine Tageseinheit schläft, erhält er den temporären Aspekt "Müde".

## Erforschen

Kann **nicht** mit Wandern kombiniert werden.

Die Erkundung eines Abenteuerorts kann von einer Tageseinheit bis zu mehreren Tagen oder sogar Wochen dauern.
