# Reisen

## Wandern

Für die Reise durch die Welt können pro Tag nur zwei Tageseinheiten genutzt werden, mit Ausnahme, es wird ein Gewaltmarsch, durchgeführt.

```admonish info
Man kann im einfachen Gelände zwei Hexfelder pro Tageseinheit und in schwierigem Gelände ein Hexfeld pro Tageseinheit gehen. Zu Pferd erhöht sich die Bewegungsrate auf drei Hexfelder pro Tageseinheit in einfachem Gelände.
```

Kurze Pausen sind in der angegebenen Zeitangabe enthalten, aber wenn man z.B. wegen eines Missgeschicks anhalten muss - besteht die Gefahr, dass die Strecke nicht mehr im vorgegebenen Zeitraum zurückgelegt werden kann. Der\*die Spielleitung hat das letzte Wort.

### Gewaltmarsch

Wenn mehr als zwei Tageseinheiten gewandert werden soll, ist das mit erhöhter Anstrengung verbunden. Dies erfordert eine Bewegung-Probe, für jede weitere Tageseinheit benötigt es einen weiteren Erfolg. Für jede misslungene Probe gibt es einen Punkt Stress. Wenn nicht alle Charaktere die Probe geschafft haben, kann die Gruppe sich aufzuteilen und die Nachzügler zurücklassen oder einen Punkt Stress in Kauf nehmen, um Schritt zu halten.

### Transportmittel

Fortbewegungsmittel, wie Pferde oder Kutschen, erlauben es schneller durch einfaches Gelände zu reisen. Die Fortbewegungsgeschwindigkeit pro Tageseinheit entspricht dann drei Hexfeldern.
Gewaltmärsche sind ebenfalls möglich.

## Navigator

Das Gruppenmitglied, welches sich um die Wegfindung kümmert, wird Navigator genannt. Es kann in der Gruppe pro Tageseinheit nur einen Navigator geben, der in diesem Zeitraum auch nicht gewechselt werden kann. Die Navigation kann während des Wanderns erfolgen. Bei jedem Hexfeld, das durchquert werden soll, muss der Navigator eine Wahrnehmungs-Probe ablegen, die Schwierigkeit kann durch äußere Einflüsse erschwert werden. Bei einem Misserfolg wird auf die [Tabelle Misserfolg Navigation](/grundregelwerk/zeiteinteilung/zufallstabelle.md#misserfolg-navigation) gewürfelt.

### Dunkelheit

Das Reisen in der Dunkelheit birgt viele Gefahren, z.B. sich zu verirren oder auf gefährliche Tiere zu stoßen. Der Navigator bekommt während der Dunkelheit einen Nachteil

### bekannte Felder

Wenn die Gruppe ein bekanntes Hexfeld durchquert, muss der Navigator nicht würfeln.

## Wache

Kann nur ein Gruppenmitglied übernehmen und mit Wandern kombiniert werden.

Das Gruppenmitglied, welches sich um das Auskundschaften von Bedrohungen und Feinden kümmert, wird Wache genannt. Es kann pro Tageseinheit nur eine Wache geben, die innerhalb dieses Zeitraums auch nicht gewechselt werden kann. Bei jedem Hexfeld, das durchquert wird, muss die Wache eine Wahrnehmungs-Probe ablegen. Die Schwierigkeit kann durch äußere Einflüsse erschwert werden. Bei einem Erfolg kann die Bedrohung frühzeitig bemerkt werden, sodass sie z.B. umgangen werden kann. Bei einem Misserfolg wird die Gruppe von der Bedrohung überrascht.

```admonish tip
Während des Marsches wird einmal pro Tageseinheit gewürfelt, ob eine Begegnung stattfindet. Wenn die Gruppe im Hexfeld verweilt, wird einmal für den ganzen Tag gewürfelt.
```

## Nahrungsbeschaffung

Die Nahrungsbeschaffung kann von mehreren Gruppenmitgliedern übernommen werden, allerdings können die entsprechenden Gruppenmitglieder in dieser Zeit aber nicht wandern.

Es kann eine Tageseinheit damit verbracht werden, Nahrung oder Trinkwasser zu beschaffen. Es können alle in der Gruppe einzeln suchen oder auch gemeinsam. Bei einzelnen Aktionen würfelt jeder Charakter einzeln, wenn als Gruppe gesucht wird, bekommt der Anführer einen Vorteil. Die Nahrungsbeschaffung wird in folgende Unterpunkte aufgeteilt: Sammeln, Jagen und Fischen. Pro Tageseinheit kann nur eine dieser Tätigkeiten ausgeübt werden.
Gefundene Nahrung, die nicht innerhalb eines Tages verbraucht oder verarbeitet wird, verdirbt und ist dann nicht mehr genießbar.

### Jahreszeiten

Die [Jahreszeiten](/grundregelwerk/zeiteinteilung/index.md#jahreszeiten) beeinflussen die Erfolgswahrscheinlichkeit, Nahrung zu finden. Folgende Modifikatoren werden auf die Wahrnehmungs-Probe angewendet.

| Jahreszeit | Modifikator |
| ---------- | ----------- |
| Frühling   | Nachteil    |
| Sommer     | 0           |
| Herbst     | Vorteil     |
| Winter     | Nachteil     |

### Sammeln

Es ist eine Wahrnehmungs-Probe erforderlich, um Nahrung, in diesem Fall Wasser, Gemüse, Wurzeln, Pflanzen oder Pilze, für sich selbst zu finden. Für jeden weiteren Schwierigkeitsgrad, der erreicht wurde, wird Nahrung für eine weitere Person gefunden. Falls die Probe misslingt, würfelt auf die [misserfolg Sammeln Tabelle](/grundregelwerk/zeiteinteilung/zufallstabelle.md#misserfolg-sammeln).

### Jagen

Es ist eine Wahrnehmungs-Probe erforderlich, um Nahrung, in diesem Fall Fleisch, zu finden. Bei einer erfolgreichen Probe wird auf [beute Jagen Tabelle](/grundregelwerk/tabellen_werte/index.md#beute-jagen) gewürfelt. Falls die Probe misslingt, würfelt auf die [misserfolg Jagen Tabelle](/grundregelwerk/zeiteinteilung/zufallstabelle.md#misserfolg-jagen). Gegen das Tier muss nicht gekämpft werden, eine erfolgreiche Angriffs-Probe reicht aus, um es zu erlegen, auch diese kann durch Einflüsse erschwert sein z.B. wenn keine geeignete Waffe geführt wird.

#### Beute Jagen

| d10 | Tier        | Fleisch | Pelz |
| --- | ----------- | ------- | ---- |
| 1   | Hirsch      | 5       | 3    |
| 2   | Wildschwein | 4       | 2    |
| 3   | Fuchs       | 3       | 1    |
| 4   | Hase        | 2       | 1    |
| 5   | Maus        | 1       | 0    |
| 6   | Krähe       | 3       | 0    |
| 7   | Hase        | 2       | 1    |
| 8   | Fuchs       | 3       | 1    |
| 9   | Wildschwein | 4       | 2    |
| 10  | Reh         | 5       | 3    |

### Fischen

Benötigt einen Fluss oder einen See und entsprechende Ausrüstung.

Es ist eine Fachkenntnisse-Probe erforderlich, um Fische zu fangen. Für jeden weiteren Schwierigkeitsgrad, der erreicht wurde, wird Nahrung für eine weitere Person gefunden. Wenn ihr den Fluss per Boot überquert, könnt ihr währenddessen angeln. Falls die Probe misslingt, würfelt auf die [misserfolg Fischen Tabelle](/grundregelwerk/zeiteinteilung/zufallstabelle.md#misserfolg-fischen).
