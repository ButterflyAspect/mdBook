# Zeiteinteilung

## Der Tag

Um die Verwaltung zu vereinfachen, wird der Tag geviertelt und in folgende Zeitabschnitte eingeteilt: morgens, nachmittags, abends, nachts. Folgend wird ein Vierteltag als Tageseinheit bezeichnet.
Zu Beginn einer jeden Tageseinheit wird entschieden, welche Aufgaben der Charakter in dieser Tageseinheit tun möchte. Welche Aktivitäten einzeln oder als Gruppe ausgeführt werden können, werden in den einzelnen Punkten erklärt.

### Jahreszeiten

| Frühling | Sommer | Herbst | Winter |
| -------- | ------ | ------ | ------ |
| hell     | hell   | hell   | dunkel |
| hell     | hell   | hell   | hell   |
| dunkel   | hell   | dunkel | dunkel |
| dunkel   | dunkel | dunkel | dunkel |
