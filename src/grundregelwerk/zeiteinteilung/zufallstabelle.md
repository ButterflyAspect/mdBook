# Zufallstabellen

## Misserfolg Navigation

| d10 | Effekt |
|-----|--------|
| 1   | nichts |
| 2   | nichts |
| 3   | Ihr sind in einen Bereich mit Treibsand gelaufen. Um zu entkommen, muss eine schwere Probe abgelegt werden. Wenn sie scheitert, erleiden der Charakter 1 Punkt Schaden und es kann erneut gewürfelt werden. Wer sich befreit, kann allen helfen, die noch festsitzen. Da dieses tückische Gebiet umgangen werden muss, kommt man während dieser Tageseinheit nicht weiter. |
| 4   | Der weitere Weg ist durch Felsen, umgestürzte Bäume, dichtes Gestrüpp oder Überschwemmungen versperrt (je nach Geländetyp im Feld). Es muss eine schwere Probe abgelegt werden, um den Weg freizumachen oder zu durchqueren. Dieses Gebiet kann umgangen werden, dies kostet euch allerdings eine Tageseinheit. |
| 5   | Ihr stellt fest, dass ihr im Kreis gelaufen seid, dies kostet euch eine Tageseinheit. Um euren Weg wiederzufinden, muss der Navigator eine normale Probe schaffen. |
| 6   | Der Navigator stürzt und verletzt sich das Knöchel; er muss sich für eine Tageseinheit ausruhen, bevor er das Bein wieder belasten kann. |
| 7   | Ein wildes Tier fühlt sich bedroht und greift die Gruppe an. |
| 8   | Ein massiver Regen- oder Schneesturm (je nach Jahreszeit) erwischt die Gruppe unvorbereitet. Es muss sich ein Unterschlupf gesucht werden oder die Gruppe erleidet 1d10 Schaden. Die Gruppe kommt für diese Tageseinheit nicht weiter. |
| 9   | nichts |
| 10  | nichts |

## Misserfolg Sammeln

| d10 | Effekt |
|-----|--------|
| 1   | nichts |
| 2   | nichts |
| 3   | Die gefundene Nahrung ist Giftig, was aber erst nach dem Verzehr und eine Tageseinheit später festgestellt wird. Es plagt den Charakter Übelkeit und Durchfall. Nachteil auf alle Proben. |
| 4   | Blutegel beißen sich in das Fleisch des Charakters und er erleiden 1d3 Punkt Schaden. Die Blutegel können durch eine normale Medizin-Probe entfernt werden. Wenn die Probe misslingt, erleiden der Charakter weitere 1d3 Punkte Schaden. |
| 5   | Der Charakter stürzt und erleidet eine Verletzung, die das Bewegungstempo auf 1 Hexfeld pro Tag beschränkt. Durch eine normale Medizin-Probe entfällt der Malus nach einem Tag wieder, ansonsten dauert er drei Tage an. |
| 6   | Die Kleidung wird beschädigt. Im Frühling, Herbst und Winter erleidest du einen Malus von Nachteil auf alle Proben bis deine Kleidung repariert (normale Fachkenntnisse-Probe) oder getauscht wurde. |
| 7   | Ein wildes Tier fühlt sich bedroht und greift den Charakter an. |
| 8   | Ein Eichhörnchen, ein Vogel oder ein anderes kleines Tier folgt dem Charakter. Das Tier verursacht Ärger z.B. könnte zu einer unpassenden Zeit Lärm machen, Ihr Essen essen oder etwas stehlen. |
| 9   | nichts |
| 10  | nichts |

## Misserfolg Jagen

| d10 | Effekt |
|-----|--------|
| 1   | nichts |
| 2   | nichts |
| 3   | Der Charakter stürzt und erleidet eine Verletzung, die das Bewegungstempo auf 1 Hexfeld pro Tag beschränkt. Durch eine normale Medizin-Probe entfällt der Malus nach einem Tag wieder, ansonsten dauert er drei Tage an. |
| 4   | Der Charakter verliert etwas von seiner Jagdausrüstung |
| 5   | Die Kleidung wird beschädigt. Im Frühling, Herbst und Winter erleidest du einen Malus von Nachteil auf alle Proben bis deine Kleidung repariert (normale Fachkenntnisse-Probe) oder getauscht wurde. |
| 6   | Der Charakter tritt in eine Falle eines anderen Jägers, würfel 1d5 Schaden und befreie dich mit einer entsprechenden Probe aus der Falle. |
| 7   | Ein wildes Tier fühlt sich bedroht und greift den Charakter an. |
| 8   | Das erlegte Tier ist Krank, was aber erst nach dem Verzehr und eine Tageseinheit später festgestellt wird. Es plagt den Charakter Übelkeit und Durchfall. Nachteil auf alle Proben. |
| 9   | nichts |
| 10  | nichts |

## Misserfolg Fischen

| d10 | Effekt |
|-----|--------|
| 1   | nichts |
| 2   | nichts |
| 3   | Der Haken oder das Netz verfängt sich, mit einer schweren Körperkraft-Probe kann die Ausrüstung gerettet werden. |
| 4   | Der Haken bohrt sich in den Finger des Charakters und er erhält 1 Schaden. |
| 5   | Die Angelausrüstung geht kaputt und muss mit einer normalen Fachkenntnisse-Probe repariert wird. |
| 6   | Ein großer Schwarm von Moskitos oder Mücken greift den Charakter an und verursachen 1d5 Schaden. |
| 7   | Der Charakter verliert das Gleichgewicht und fällt ins Wasser. |
| 8   | Ein bösartiger Fisch oder Aal greift den Charakter an und verursacht 1d5 Schaden. |
| 9   | nichts |
| 10  | nichts |

## Misserfolg Lager

| d10 | Effekt |
|-----|--------|
| 1   | Ihr Lager liegt mitten in einer Ameisenstraße. Die Gruppe erhält keine Erholung. |
| 2   | Ein zufällig ausgewählter Charakter hat sich Läuse eingefangen. Es juckt furchtbar und er bekommt einen Ausschlag am ganzen Körper. Das Opfer erleidet jeden Tag 1 Punkt Schaden und kann nicht Schlafen. Eine normale Medizin-Probe stoppt den Effekt. |
| 3   | Das Wasser ist verdorben. Jeder in der Gruppe muss sich neues Wasser besorgen. |
| 4   | Ihr Essen ist verrottet oder von Insekten befallen. Jeder in der Gruppe muss neues Essen besorgen. |
| 5   | Der Lagerplatz stellt sich als ungeeignet heraus, keiner in der Gruppe bekommt erholsamen Schlaf. |
| 6   | Ein Sturm sucht das Lager heim, alles ist durchnässt und in der Gruppe bekommt erholsamen Schlaf. |
| 7   | Das Feuerholz ist nass und lässt das Feuer erlöschen. |
| 8   | Die Flammen Ihres Lagerfeuers geraten außer Kontrolle. Die Zelte, Schlafplätze und Ausrüstung fangen Feuer. Jeder in der Gruppe erleidet 1d10 Schaden. Jeder, der seine Ausrüstung retten möchte, muss eine Bewegungs-Probe schaffen. |
| 9   | Ein zufälliger Charakter verliert einen Gegenstand. |
| 10  | Ein zufälliger Gegenstand eines Charakters geht kaputt. Der Gegenstand kann mit einer Fachkenntnisse-Probe repariert werden. |

## Misserfolg Seereise

| d10 | Effekt |
|-----|--------|
| 1   | nichts |
| 2   | nichts |
| 3   | Sie segeln vom Kurs ab und machen während dieser Tageseinheit keinen Fortschritt auf der Karte. |
| 4   | Eine plötzliche Sturmböe lässt Ihr Boot plötzlich kippen. Ein wichtiger Gegenstand fällt ins Wasser. |
| 5   | Das Boot ist in einem Strudel gefangen. Es muss eine Fachkenntnisse-Probe abgelegt werden. Scheitern bedeutet, dass das Boot auf Grund läuft und mit einer schweren Fachkenntnisse-Probe repariert werden, bevor die Reise fortgesetzt werden kann. |
| 6   | Ihr Boot hat ein Leck und nimmt Wasser auf. Das Leck muss mit einer normalen Fachkenntnisse-Probe repariert werden (ein Fachkenntnisse-Probe), Ihre Reise kann währenddessen weitergehen. Wenn das Leck nicht repariert wird, sinkt das Boot nach 1d10 Stunden. |
| 7   | Ein Charakter der Gruppe fällt über Bord, nachdem eine große Welle das Boot getroffen hat. |
| 8   | Ihr Boot läuft auf Grund und muss aufgegeben oder mit einer Fachkenntnisse-Probe repariert werden, falls Werkzeug und Material vorhanden. Das Boot muss am Ufer liegen, um repariert zu werden. |
| 9   | nichts |
| 10  | nichts |
