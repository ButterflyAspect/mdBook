# Schub

Ein Schub ist ein Modifikator, der auf eine bestimmte Situation oder Aktion angewendet wird und nach einmaliger Anwendung oder Ablauf der Situation oder Aktion wieder verschwindet. Für einen Schub muss kein Schicksalspunkt ausgegeben werden. Er bringt normalerweise einen Vorteil.

```admonish example title='Beispiel'
Susanne würfelt eine erfolgreiche Einflussnahme-Probe, um dem Gegner den Schub "Ablenkung" zu geben. Wenn ein Teammitglied den Gegner jetzt angreift, könnte es den Schub nutzen und einen Vorteil beim Angriff erhalten.
```

## Schübe sind keine Aspekte

Ein Schub ist kein vollwertiger Aspekt - du kannst ihn nicht reizen, du kannst ihn nicht als Voraussetzung für Extras verwenden, du kannst ihn nicht mit einem Schicksalspunkt wieder einsetzen und du kannst keine anderen Dinge mit ihm machen, die einen Aspekt beeinflussen oder von einem Aspekt beeinflusst werden: Du kannst ihn nicht gegen den Charakter einsetzen, der mit ihm verbunden ist.

## Schübe sind nicht kumulativ

Schübe addieren sich nicht, ebenso wenig können sie zu Aspekten addiert werden, wenn ein Schub/Aspekt bereits aktiv ist, wird dieser überschrieben.
