# Aspekte

## Was sind Aspekte

Ein Aspekt ist ein Ausdruck, der eine einzigartige oder bemerkenswerte Eigenschaft einer Sache oder Person beschreibt. Wenn du Schicksalspunkte ausgeben möchtest, kannst du das am besten, indem du Aspekte einsetzt. Außerdem beeinflussen Aspekte die Geschichte, da sie dir die Möglichkeit geben, einen Modifikator für deinen Charakter zu erhalten. Der größte Unterschied zu den Stunts ist, das Aspekte keine Werte besitzen oder beinflussen.

## Aspekte einsetzen

Du kannst Aspekte immer dann einsetzen, wenn du in einer Situation das Gefühl hast, dass einer deiner Aspekte für deinen Charakter nützlich ist.
Um einen Aspekt einzusetzen, musst du begründen, warum der Aspekt relevant ist und einen Schicksalspunkt ausgeben. Der Einsatz eines Aspekts muss sinnvoll sein, oder du musst ihn kreativ genug beschreiben können, um ihn sinnvoll zu machen. Dann kannst du einen der folgenden Vorteile wählen:

- einen Vorteil auf deine aktuelle Fertigkeitsprobe (muss vorher angesagt werden)
- einen Vorteil auf die Probe eines anderen Charakters, wenn die Situation und der Aspekt es erlauben (muss vorher angesagt werden)
- erneut würfeln
- einen Patzer vermeiden

Für die Vorteile muss man sich vor der Probe entscheiden. Wenn du noch einmal würfelst, zählt das zweite Ergebnis. Du kannst mehrere Aspekte in einer Probe nutzen, aber du kannst denselben Aspekt nicht mehr als einmal in einer Probe nutzen.

### Mit Aspekten Fakten schaffen

Du kannst einen Aspekt auch nutzen, um einen Fakt in eurer Geschichte zu schaffen. Dabei übernimmst du kurz die Kontrolle über die Spielwelt und erschaffst ein neues Detail, das dir oder deinen Mitspielern nützt. Dies ist nicht mit einer Probe verbunden, kostet dich aber einen Schicksalspunkt. Du könntest z.B. deinen Aspekt Szenekenntnis nutzen und festlegen, dass eine Geheimorganisation eine Untergruppe in der Stadt hat, in der sich eure Charaktere gerade befinden.
Fakten schaffen hängt stark davon ab, welchen Aspekt du einsetzt.
Das Erschaffen von Fakten unterliegt den gleichen Einschränkungen wie [Kleinere Fakten schaffen](/grundregelwerk/schicksalspunkte/index.md#kleinere-fakten-schaffen). Die Spielleitung kann ein Veto einlegen, sollte dies aber gut überlegen. Normalerweise verfolgen die Spieler einen bestimmten Zweck, wenn sie einen Fakt erschaffen.

```admonish example title='Beispiel'
Djego geht einem Gerücht nach, dass ihm ein lokaler Journalist bei einer heißen Spur helfen könnte. Als er im Büro der Zeitung nachfragt, wird er abgewiesen. Es scheint, als sei der Journalist bedroht worden. Er versucht nun, sich zu schützen, indem er mit niemandem spricht. Peter will seinen Aspekt "Meine geliebte Frau starb bei einem Unfall mit Fahrerflucht" nutzen, um eine Tatsache zu schaffen.

  Er erklärt, dass der Journalist damals über den Unfall berichtete, die beiden sich also kannten und mochten.

  Obwohl der Spielleiter dieses Detail nicht geplant hatte, stimmt er Peters Beschreibung der Tatsache zu, da sie einen guten Einblick in den Hintergrund des Charakters gibt. Peter bezahlt den Schicksalspunkt. Später spielt der SL den Journalisten, der Djego erkennt und ihm deshalb helfen wird.
```

### Den Tod betrügen

Für 1 Schicksalspunkt und einen passenden Aspekt kann man den Tod seines Charakters verhindern.

### Einen Aspekt reizen

Sowohl du als auch die Spielleitung können Aspekte nutzen, um das Verhalten deines Charakters zu beeinflussen. Du kannst deine eigenen Aspekte reizen, um die Handlung deines Charakters voranzutreiben oder interessante Wendungen in der Geschichte zu erzeugen. Die Spielleitung kann deine Aspekte nutzen, um dich zu bestimmten Handlungen zu motivieren oder die Handlung spannender zu gestalten.

Das Reizen von Aspekten kann dazu führen, dass du einen Nachteil bekommst oder eine Aktion automatisch scheitert. Bist du jedoch bereit, die Stimulation zuzulassen, kannst du dafür einen Schicksalspunkt erhalten. Letztendlich liegt es an dir, wie du mit den Aspekten deines Charakters interagierst und wie du sie in der Geschichte einsetzt.

```admonish example title='Beispiel'
Der Spielleiter lässt Jimmy, den kleinen Kriminellen, einen Fluchtversuch unternehmen, genau wie Peter es verlangt hat. Jimmy wirft den Tisch um und schiebt sich an Djego vorbei durch die Tür. Peter weiß, dass seine Verstärkung im hinteren Teil der Kneipe Wache steht, also verfolgt er Jimmy selbst nach vorne. Jimmy ist jung und flink, und der Anführer reizt Djegos „Ich bin zu alt für diesen Scheiß“. Sie schiebt einen Schicksalspunkt in Peters Richtung und schlägt vor, dass Djego nur zwei Runden Zeit hat, um Jimmy zu fangen. Dann muss er, völlig außer Atem, aufgeben. Peter lässt sich provozieren und erhält den angebotenen Schicksalspunkt. Es wird sich schon eine andere Möglichkeit finden, Jimmy zu fangen ...
```

#### Den Reizen widerstehen

Wenn du nicht willst, dass ein Aspekt gereizt wird, kannst du ablehnen. Das kostet aber einen Schicksalspunkt (anstatt einen zu bekommen).

```admonish example title='Beispiel'
Alison hat den Aspekt „Stecke deine Nase in Dinge, die dich nichts angehen“. Sie ist einem Verdächtigen in ein Kellergewölbe gefolgt und hört den vielstimmigen Gesang einer dunklen Sekte, die gerade ein Ritual vollzieht. Der Spielleiter hat vorher angedeutet, dass diese Leute ziemlich gefährlich sind, aber Alison steckt ihre Nase eben gerne in Dinge, die sie nichts angehen, und der Spielleiter bietet Hanna einen Schicksalspunkt an, wenn Alison sich allein und ohne Unterstützung ihrer Freunde in Lebensgefahr begibt. Das ist Hanna zu riskant und sie widersteht der Versuchung. Dafür muss sie allerdings einen Schicksalspunkt abgeben.
```
