# Schwächen

Schwächen sind alles, was einem Charakter das Leben schwer macht. Primäre Schwächen sind solche, die die Handlungen einer Figur beeinflussen oder negative Reaktionen der Umwelt hervorrufen. Verschiedene Einstellungen, Neurosen und Phobien sind Schwächen, ebenso wie körperliche Behinderungen und soziale Stigmata. Es gibt auch heroische Schwächen: wenn ein Ehrenkodex und die Unfähigkeit zu lügen die Möglichkeiten erheblich einschränken, aber keine signifikante Schwäche einer schwachen Persönlichkeit sind.

## Beispiele

- Häufig Geistesabwesend
- Abhängigkeit
- ambitionierter Herzensbrecher
- Blutgier
- Roh und Taktlos
- Muss immer das letzte Wort haben
- Ethischer Kodex, der die Entscheidungsfreiheit beschränkt
- Ehrenkodex
- zwanghaftes Verhalten
- Leicht zu verwirren
- mächtiger Feind
- aufgeblähter Macho
- Gierig
- Humanitär (hilft ohne Bezahlung)
- realitätsfremder Idealist
- Unentschlossenheit
- Intolerant
- Eifersüchtig auf xxx
- Faul
- Manisch-Depressiv
- Melancholisch
- Schizophren
- Absolute Gehorsamkeit
- Besessenheit
- Anarchist
- Übermütig
- Über zuversichtlich
- Phobien
- Pazifist
- sozial ungeschickt
- weichherzig
- Stur
- leicht reizbar
- eitel
- schnell gewalttätig
- Treueschwur
- Verängstigt
