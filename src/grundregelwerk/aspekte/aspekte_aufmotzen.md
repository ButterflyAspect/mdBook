# Aspekte aufmotzen

Aspekte sind die Dreh- und Angelpunkte des Spielercharakters; sie sind das erste, was der Spielleiter auf dem Charakterbogen sieht, wenn er sich fragt, welche Abenteuer ihr demnächst erleben werdet. Das macht sie zu mächtigen Werkzeugen, über die du die volle Kontrolle hast und die du genau nach deinen Wünschen gestalten kannst.

Jedes Mal, wenn du einen Aspekt in den Charakterbogen einträgst, solltest du dich fragen: _Hat dieser Aspekt genug Atmosphäre?_ Wenn die Antwort nicht _Ja!_ lautet, musst du ihn vielleicht noch etwas aufpeppen. Nicht jeder Aspekt muss vor Witz und Einfallsreichtum strotzen, aber ein farbloses Charakterkonzept bietet der Spielleitung kaum Möglichkeiten, den Spielercharakter interessant in die Spielhandlung einzubinden und sich von ihr inspirieren zu lassen.

## Beispiele

| Lahm                 | Lecker                | Bamm!                       |
| -------------------- | --------------------- | --------------------------- |
| Stark                | Stark wie ein Ochse   | Mann aus Eisen              |
| Dunkle Vergangenheit | Ehemaliger Sektierer  | Auge von Anubis             |
| Schwertkämpfer       | Ausgebildeter Krieger | Ausgebildet von Montcharles |

In allen Beispielen beschreibt der **lahme** Name den Aspekt bereits so, dass man erkennen kann, wie er verwendet werden soll. Man kann aber nicht sagen, dass er dem Leser direkt ins Auge springt und ihm sofort eine Menge Geschichten einfallen.

Die **leckeren** Namen sind schon besser, weil sie konkreter sind und sowohl den Spielern als auch der Spielleitung Ansatzpunkte für Storyideen liefern.

Aber die **Bamm!**-Aspekte sind das, worum es gehen sollte.

**Mann aus Eisen**
könnte tatsächlich der Name sein, den die anderen der Spielerfigur gegeben haben. Jedenfalls beschreibt dieser Name nicht nur einen starken Kerl, sondern lässt noch viele andere Assoziationen zu.

**Auge von Anubis**
könnte die Sekte sein, der die Spielerfigur einst angehörte. Der Aspekt schickt die Spielleitung nach Ägypten, um von dort Storyideen mitzubringen und packt auch gleich ein paar NSCs auf den Tisch

**Ausgebildet von Montcharles**
lässt den Spielercharakter oft an die Zeit mit Pierre Montcharles zurückdenken, in der es nicht nur um Unterricht und Geschichte rund ums Fechten geht. Es ist auch möglich, dass Pierre irgendwann in der Geschichte auftaucht.

## Fazit

Bei der Wahl eines Aspektes sollte man sich immer fragen, ob der Aspekt Bamm! ist, oder nur lahm oder lecker.
