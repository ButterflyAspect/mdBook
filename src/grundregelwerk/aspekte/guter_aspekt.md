# Was ist ein guter Aspekt?

Es kann ziemlich einfach sein, sich einen Aspekt auszudenken. Aber es ist ein bisschen schwieriger, Aspekte zu finden, die man benutzen, ausnutzen und herausfordern kann! \
Aspekte haben verschiedene Funktionen im Spiel. Ein guter Aspekt ist einer, der alle erfüllt.

## Ist er packend?

Ein Aspekt sollte etwas darüber aussagen, wie dein Charakter in die Welt passt. Er kann als Aufhänger für eine Geschichte dienen: Ein bestimmter Nebencharakter, eine Organisation oder ein Ereignis aus der Vergangenheit - etwas oder jemand, das für deinen Charakter wichtig ist oder ihn geprägt hat.

```admonish example title='Beispiel'
Peter will einen Aspekt haben, der Djego als hervorragenden Arzt zeigt. Anstatt einfach den Aspekt Arzt oder ausgebildeter Arzt zu wählen, nimmt er ausgebildet von Dr. House.

  Der Name kommt von einer TV-Serie. Der Arzt dort löst quasi jeden Fall. Peter zeigt, dass sein Charakter vom Besten unterrichtet wurde und er somit ein kluger, aufmerksamer und brillanter Arzt ist. Besser noch, er führt so eine Nebenfigur ein, die die Spielleitung verwenden könnte, um Djego in eine Geschichte zu verwickeln oder Peter kann ihn mit Hilfe seines Aspekts auftauchen lassen.
```

## Kann er eingesetzt werden?

Um herauszufinden, ob dein Aspekt im Spiel eingesetzt werden kann, solltest du versuchen, zwei oder drei verschiedene Situationen zu finden, in denen du ihn einsetzen kannst.
Wenn du Schwierigkeiten hast, mehr als eine Situation zu finden, ist der Aspekt zu eng gefasst.

```admonish example title='Beispiel'
"Sagt zu einem Drink nicht Nein" scheint weitgehend eine Schwäche zu sein. Aber Peter kann ihn auch benutzen, um mit der Flasche Whisky, die Djego immer bei sich hat, einen Nichtspielercharakter zu bestechen oder sich einen Molotow-Cocktail zu bauen.

  Er könnte auch eingesetzt werden, um eine Personen-Probe zu steigern, weil Djego alle Pubs und Bars in der Gegend kennt – auch jene, die ein Verdächtiger regelmäßig besucht. Ebenso könnte der Aspekt eine List-Probe verbessern, um bei einer Observation in einer Bar unentdeckt zu bleiben.
```

## Kann er gereizt werden?

Ein guter Aspekt ist wie ein zweischneidiges Schwert. In manchen Situationen verschafft er dir einen Vorteil, in anderen behindert er dich. In manchen Situationen bringt er dir Schicksalspunkte, in anderen kostet er dich Schicksalspunkte.
Wenn der Aspekt irritiert ist, sollte das deinen Charakter in interessante Situationen bringen. Wenn du akzeptierst, dass einer deiner Aspekte gereizt wird, führt das oft zu einer spannenderen Geschichte.

```admonish example title='Beispiel'
Djegos Aspekt „Ich will Ergebnisse!“ könnte gereizt werden, um ihn dazu zu bringen, die zweifelhafte Aussage eines Verbrechers zu akzeptieren, damit er schneller jemanden verhaften kann – auch wenn Peter weiß, dass die Aussage wahrscheinlich falsch ist und ihn in Schwierigkeiten mit einem lokalen Bandenchef bringen wird.

  Peter könnte seinen Aspekt selbst reizen und erklären, dass Djego einem Verdächtigen Beweise unterschiebt, um eine Verurteilung zu erreichen. Dabei ist ihm aber klar, dass die Täuschung früher oder später auffliegen und Djego in Schwierigkeiten bringen wird.
```

Wenn du keinen Aspekt findest, den man einsetzen und reizen kann, dann gib deinem Charakter einige Aspekte, die man nur einsetzen kann, und einige, die nur gereizt werden können. Das Verhältnis zwischen diesen Aspekten sollte ausgewogen sein.

## Mach hin und diskutier später

Deine Aspekte sollten kurz sein - einprägsame Sätze, keine Absätze oder Textpassagen. Die Kürze eines Aspekts bedeutet jedoch auch, dass einige Dinge unausgesprochen bleiben. Du solltest dann erklären, was du dir dabei gedacht hast, damit die Diskussionsleitung die genaue Bedeutung des Aspekts versteht.

```admonish example title='Beispiel'
Die Spielleitung ist mit den Dr. House TV-Serien nicht vertraut. Peter erklärt ihr, was es bedeutet, wenn jemand von Dr. House ausgebildet wurde.
```

Aspekte sind auch eine Möglichkeit zu zeigen, was du im Spiel sehen möchtest. Wenn du den Aspekt "todesmutig" nimmst, willst du Geschichten erleben, in denen dein Charakter ständig in lebensbedrohliche Situationen gerät.
Als Spielleiter solltest du besonders auf die Charakteraspekte achten, denn sie geben dir Aufschluss darüber, was deine Spieler erleben wollen.
