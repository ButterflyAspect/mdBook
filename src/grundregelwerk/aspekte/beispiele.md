# Beispiele

## Allgemeines

- Immer in Eile
- „Aber bei mir funktioniert das!“
- Neugier ist der Katze Tod.
- „Kenne ich dich nicht von irgendwoher?“
- Immer der Erste
- Ein Mädchen in jedem Hafen
- Herz aus Gold
- Mir geht eine Lüge leicht von der Zunge
- Traue keinem, der dir nicht in die Augen schauen kann
- Merkwürdige Zufälle
- Ein Paparazzo verfolgt jeden meiner Schritte
- Mucksmäuschenstill
- Rücksichtlos, wenn er seinen Spaß hat
- Will etwas beweisen
- „Wir brauchen mehr! Viel mehr ... und größer!“
- „Meine Eltern bezahlen das schon“
- „Mein Bauernhof läuft gut“
- Kind von Drachentöter Nathaniel
- Rechte Hand von König Kruze
