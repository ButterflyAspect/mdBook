# Charakterentwicklung

## Steigerung

Die gewonnenen Erfahrungspunkte können zum Steigern der Fertigkeits- und Attributswerte verwendet werden. In der Tabelle siehst du die Steigerungskosten.

```admonish example title='Beispiel'
Wenn du z.B. Vitalität von 1 auf 3 steigern möchtest, benötigst du dafür 50 EP (20 EP für Stufe 2 + 30 EP für Stufe 3).
```

| Art                                                                          | EP                        | Ressourcen |
| ---------------------------------------------------------------------------- | ------------------------- | ---------- |
| [Attribute](/grundregelwerk/attribute_fertigkeiten/index.md#attribute)       | Stufe x10                 | -          |
| [Fertigkeiten](/grundregelwerk/attribute_fertigkeiten/index.md#fertigkeiten) | Stufe x5                  | -          |
| [Aspekt](/grundregelwerk/aspekte/index.md)                                   | Spielleitungsentscheidung | -          |
| [Kompetenz](/grundregelwerk/kompetenzen/index.md)                            | 30                        | -          |
| [Stunts](/grundregelwerk/stunts/index.md)                                    | 50                        | -          |

### Erfahrungspunkte

Eine Möglichkeit für Charaktere, Erfahrung zu sammeln, ist das Abschließen von Abenteuern und das Erreichen von Zielen. Die Aufklärung eines Verbrechens, die Rettung eines Dorfes vor marodierenden Monstern oder die Eroberung eines lang gesuchten magischen Artefakts sind Ziele, die den beteiligten Charakteren 5 Erfahrungspunkte einbringen sollten. Größere Taten, wie die Befreiung einer Grafschaft oder der Sieg über einen mächtigen Erzfeind, sollten mit 10 Erfahrungspunkten belohnt werden. Noch gewaltigere Unternehmungen, wie die Rettung des gesamten Königreichs vor dem drohenden Untergang, sollten mit 20 oder mehr Erfahrungspunkten belohnt werden.

Die Spielleitung kann zusätzliche Erfahrungspunkte für besonders raffinierte Ideen, die den Charakteren geholfen haben, oder für beeindruckendes Rollenspiel vergeben. Solche Situationen können mit 1-3 Erfahrungspunkten belohnt werden.

## Praxispunkte

Praxispunkte erhält man durch Erfolge, die dem Wert der Fertigkeit entsprechen.

Praxispunkte kann man bis zur Höhe des Fertigkeitswertes sammeln. Sobald dieses Maximum erreicht ist, kann man die Fertigkeit für die Hälfte der EP verbessern. Bei ungeraden EP-Kosten wird abgerundet. Danach beginnt das Sammeln der Praxispunkte wieder bei 0. Praxispunkte können auch nach Ermessen der Spielleitung vergeben werden, wenn der Spieler eine Situation besonders kreativ oder gut gelöst hat.
