# Würfe und Proben

Bei jeder Handlung mit ungewissem Ausgang wird ein passender Wert ermittelt, auf den mit Würfeln geworfen wird, in der Regel festgelegt von der Spielleitung. Der Ausgang dieses Wurfes zeigt an, ob und wie erfolgreich die Handlung war. Dies wird hier Probe genannt. Für alle Proben werden zehnseitige Würfel _d10_ verwendet.

## Anzahl der Würfel

Die Anzahl der Würfel ergibt sich aus der Summe von Attribut und Fertigkeit. Das Attribut legt fest, wie viele Würfel ihr behalten dürft, diese werden auch **geltende Würfel** genannt.

```admonish example title='Beispiel'
Bei den Werten Stärke 3 und Nahkampf 3 werden _6d10_ gewürfelt. Nehmen wir mal an, wir würfeln 5, 19, 11, 9, 6 und 5. Dann sind die geltenden Würfel 19, 11, 9 und 6.
```

```admonish info
Auch ungelernte Fertigkeiten kann man würfeln. Dafür nimmt man den Wert 0 für die Fertigkeit. Dadurch hat man einen Nachteil bei der Probe.
```

### Erfolge

Wenn ihr Erfolge haben wollt, müsst ihr auf dem Würfel eine 8 oder höher haben.

```admonish example title='Beispiel'
Bei der Probe oben haben wir also drei Erfolge erzielt, weil wir eine 19, eine 11 und eine 9 gewürfelt haben.
```

Jetzt fragt ihr euch sicher, wie es sein kann, dass ein _d10_ eine 19 oder 11 erreichen kann. Dazu kommen wir jetzt.

## Explodierende Würfel

Wenn ihr eine 10 würfelt, dürft ihr noch einmal würfeln und das Ergebnis hinzufügen. Das gilt auch für jede weitere gewürfelte 10.

```admonish info
Bei dem oben genannten Beispiel sind eine 19 und eine 11 enthalten. Das bedeutet, dass zwei Würfel eine 10 gezeigt haben und erneut gewürfelt worden sind.
```

Wir haben mit einer 19 haben wir einen kritischen Erfolg erzielt, und zwar einen _großen Erfolg_, der wie **2 Erfolge** gewertet wird. Das ergibt dann insgesamt **4 Erfolge**.

### Kritischer Erfolg

Hier findet ihr eine Übersicht, wie die kritischen Erfolge bewertet werden.

|                 | MS   | Erfolge                   |
| --------------- | ---: | ------------------------- |
| normaler Erfolg | 8    |                           |
| großer Erfolg   | 18   | einen zusätzlichen Erfolg |
| epischer Erfolg | 28   | zwei zusätzliche Erfolge  |

Wie ihr seht, wird die MS immer um 10 erhöht. So könnt ihr die Tabelle auch weiterführen.

## Misserfolg

Wenn ihr die Probe nicht schafft, heißt das nicht gleich, dass ihr versagt habt. Es kann aber sein, dass ihr zu laut gewesen seid oder dass ihr einfach länger für eure Aktion braucht.

### Kritischer Misserfolg

Wenn einer der geltenden Würfel eine Eins zeigt, ist das ein kritischer Misserfolg oder auch Patzer genannt. Das heißt, dass etwas Schlechtes passiert, zum Beispiel, dass der Dietrich beim Knacken des Schlosses abbricht. Ein kritischer Erfolg gleicht einen kritischen Misserfolg aus.

```admonish example title='Beispiel'
Bei den oben genannten Werten wird 6, 6, 1, 1, 1 und 1 gewürfelt. Damit ist unter den geltenden Würfeln eine 1 und es ist ein kritischer Misserfolg.
```

```admonish tip
Einen kritischen Misserfolg zu erreichen heißt bei unmodifizierten Würfen, dass ihr mehr Einsen würfeln müsst, als ihr Punkte im Fertigkeitswert besitzt.
```

## Schwierigkeitsgrade

Zwischen Erfolg und Misserfolg steht die sogenannte Schwierigkeit, die vom Spielleiter festgelegt wird (wenn auch nicht immer explizit genannt). Sie stellt einen Schwellenwert dar, der durch die Anzahl der Erfolge erreicht oder übertroffen werden muss. Je anspruchsvoller die Aktion, desto höher ist die Schwierigkeit.

| Benötigte Erfolge  | Schwierigkeit       |
|-------------------:|---------------------|
| 1                  | Routine             |
| 2                  | Anspruchsvoll       |
| 3                  | Schwierig           |
| 4                  | Sehr schwierig      |
| 6                  | Schier unmöglich    |
| 8                  | Verzweiflungstat    |

## Konkurrierende Proben

Wenn ihr gegen andere Charaktere antretet, müsst ihr eure Erfolge überbieten, um zu gewinnen.

## Modifikator

Modifikatoren, die durch Stunts, Aspekte oder Situationen entstehen, haben Einfluss darauf, ob ein Vorteil oder Nachteil entsteht. Das bedeutet, dass zweimal gewürfelt und das jeweils bessere oder schlechtere Ergebnis gezählt wird.

Vorteile und Nachteile gleichen sich gegenseitig aus.

```admonish example title='Beispiel'
Gunter hat im Nebel einen Nachteil bei seinen Angriffs-Proben. Aber er nutzt seinen Aspekt "Gute Kämpfer", wodurch er diesen Nachteil ausgleicht und ohne Nachteil würfeln kann.
```

### Unterstützung

Wenn ihr einem anderen Charakter helft, und einen Erfolg erzielt, bekommt dieser einen Vorteil.

## Rettungswürfe

Rettungswürfe stellen die Fähigkeit eines Charakters dar, eine unmittelbare, plötzliche Gefahr zu überwinden. Es werden so viele Würfel geworfen, wie es dem Attribut entspricht, explodierende Würfel sind nicht möglich. \
Kritische Misserfolge werden bei Rettungswürfen nicht gewertet.
