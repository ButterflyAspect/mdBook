# Schicksalspunkte

Sie können sowohl euer Glück widerspiegeln als auch göttliches Eingreifen.

## Regeneration

Wenn ihr bei Fertigkeitsproben [kritische Erfolge](grundregelwerk/index.md#kritischer-erfolg) erzielt, erhaltet ihr Schicksalspunkte in höhe dieser.

## Ausgeben

Für jeweils einen eurer Schicksalspunkte könnt ihr eine oder mehrere der folgenden Optionen nutzen.

### Bonuswürfel

Ihr könnt einen Schicksalspunkt ausgeben um einen weiteren Würfel für eure Probe zu erhalten.

### Kleinere Fakten schaffen

Ihr habt die Möglichkeit, für 1 Punkt eine Kleinigkeit im Spiel zu verändern (zum Beispiel könnt ihr damit ein Seil in einem Keller finden, wenn ihr es wollt).
Um größere Fakten zu schaffen, siehe "[Mit Aspekten Fakten schaffen](/grundregelwerk/aspekte/index.md#mit-aspekten-fakten-schaffen)".

### Aspekte nutzen

Aspekte sind beschreibende Eigenschaften deines Charakters. Sie helfen dir im Spiel: Wenn du erklärst, dass dein Charakter eine Aktion ausführt, die im Einklang mit seinem Aspekt steht, kannst du diesen Aspekt zu deinem Vorteil einsetzen.
Siehe dazu auch [Aspekte einsetzen](/grundregelwerk/aspekte/index.md#aspekte-einsetzen).

### Stunt nutzen

Einige Stunts sind sehr stark, daher kostet es einen Schicksalspunkt, sie zu aktivieren.
