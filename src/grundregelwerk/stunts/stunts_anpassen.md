# Stunts anpassen

## Schicksal-Punkte einsetzen

Am einfachsten begrenzt ihr einen zu mächtigen Stunt, indem ihr den Stunteinsatz an die Ausgabe eines Schicksal-Punktes koppelt.

```admonish example title='Beispiel'
Marco will einen Stunt, der seinem Charakter Mickey erlaubt, in jedem Konflikt als Erster handeln zu können. Der SL findet das zu mächtig, erlaubt es aber, wenn Marco bereit ist, jedes Mal einen Schicksal-Punkt dafür zu zahlen. Diesem, nun ausgewogeneren, Stunt gibt Marco den Namen "Ich bin immer der Erste!".
```

## Voraussetzungen

Stunts können andere Stunts als Voraussetzung haben – du kannst sie erst benutzen, wenn du auch den anderen Stunt beherrschst. Daher sind sie schwieriger zu bekommen, und wenn du so einen Stunt hast, bedeutet das vermutlich, dass du den Fokus auf diesen Bereich gesetzt hast – auch wenn das heißt, dass du in anderen Bereichen dafür schwächer bist. Das balanciert den mächtigeren Stunt gegenüber anderen aus.
Es ist auch möglich, eine Voraussetzung mit einem Aspekt zu verknüpfen. Die Möglichkeit, den Aspekt zu reizen, gleicht die Überlegenheit des Stunts aus.

## Verwendung pro Spielrunde

Außerdem könnt ihr einen Stunt begrenzen, indem ihr die Verwendung pro Spielrunde beschränkt.

```admonish example title='Beispiel'
Klaus SC glaubt stark an die Geisterwelt, er möchte mit „Worte im Wind“ mit der Geisterwelt reden können, um schneller an Informationen zu kommen.
```

Dass dies nicht zu mächtig wird, kann er dies nur einmal pro Spielrunde anwenden.

## Kombinierte Grenzen

Mancher Stunt ist trotz der obigen Begrenzungen noch zu mächtig. In so einem Fall wird mehr als eine Begrenzung notwendig, damit er im Gleichgewicht mit anderen Stunts ist.

```admonish example title='Beispiel'
Frank spielt einen Soldaten, der in der Lage ist, viel Schaden auszuhalten. Neben seinem hohen Wert in Vitalität überlegt sich Frank den Stunt spürt keinen Schmerz. Dieser erlaubt ihm, die Auswirkungen eines Angriffs zu ignorieren – einmal pro Spielsitzung und für einen Schicksal-Punkt. Frank findet diesen Stunt trotz der Begrenzungen äußerst nützlich und hebt ihn sich für Gelegenheiten auf, wenn ein Gegner mit einer schweren Waffe einen guten Treffer landet.
```
