# Stuntschablonen

- **Spezialisierung:** Vorteil auf eine Fertigkeit
- **Fertigkeitentausch:** Einsatz einer Fertigkeit anstelle einer anderen
- **Verbündeter:** Du erhältst einen Gefährten, den du mit Erweiterungen verbessern kannst.
- **Vorteil:** Zusätzliche Vorteile (Ressourcen, Gegenstände, spezielle Fähigkeiten).

## Spezialisierung

Eine Spezialisierung gibt deinem Charakter einen Vorteil auf eine Fertigkeit, wenn die Bedingungen stimmen. Der Anwendungsbereich hierfür kann sehr breit angelegt sein. Dennoch kommt eine Spezialisierung nicht bei jeder Fertigkeitsanwendung zur Geltung.

```admonish tip
Klingenwaffen wäre eine Spezialisierung für Kampffertigkeiten. Er kommt zum Einsatz, wenn Waffen mit einer Klinge benutzt werden wie Schwerter, Messer oder Äxte.
```

```admonish example title='Beispiel'
Peter erstellt mit dem SL die Spezialisierung Polizeispitzel für seinen Charakter. Djego bekommt einen Vorteil auf die Fertigkeit Diplomatie.
```

## Fertigkeitentausch

Diese Stuntschablone erlaubt es dir, eine Fertigkeit anstelle einer anderen zu verwenden, wenn gewisse Bedingungen erfüllt sind. Der Anwendungsbereich von Fertigkeitentausch ist ähnlich wie bei der Spezialisierung.
Ein Fertigkeitentausch kann es dir z. B. erlauben, Körperkraft anstelle von Bewegung zu verwenden, wenn du ein Tier reitest.

```admonish example title='Beispiel'
Gemeinsam entwickeln Peter mit der Spielleitung den Stunt Fertigkeitentausch Umgebung Lesen. Damit kann Peter Wahrnehmung anstelle von Diplomatie benutzten, um die Gefühle von Personen zu lesen. Dazu muss die betroffene Person in ihrem eigenen Haus, an ihrem Arbeitsplatz, in ihrem Auto oder an ihrem bevorzugten Aufenthaltsort befragt werden. Wird die gleiche Person an einem anderen Ort befragt, muss Peter seinen Diplomatie-Wert verwenden.
```

## Verbündeter

Durch diesen Stunt erhältst du einen Verbündeten, also jemanden, der dir innerhalb und außerhalb von Konflikten hilfreich zur Seite stehen kann. Ein Verbündeter ist in der Regel ein [Gefährte](/material/stuntbeispiele/index.md#gef%C3%A4hrte) bzw. [Tiergefährte](/material/stuntbeispiele/index.md#tiergef%C3%A4hrte).

## Vorteil

Ein Vorteil-Stunt ist eine allgemeine Vorlage für all jene Stunts, die nicht mit einer der vorangegangenen vier Schablonen abgedeckt werden. Mit einem Vorteil kann dein SC Zugang zu Ressourcen, Gegenständen, anderen speziellen Fähigkeiten und Eigenschaften erhalten.
Manche Vorteil-Stunts mögen euch ziemlich stark erscheinen – aber wenn ihr euch alle einig seid, dann könnt ihr sie natürlich gern verwenden. Wenn ihr aber davon ausgeht, dass sie Ungleichgewicht ins Spiel bringen, lasst sie weg oder verändert sie so, dass sie in eure Spielwelt passen. Einige Möglichkeiten, wie ihr Stunts anpassen könnt, findet ihr im Folgenden beschrieben.
