# Charaktererstellung

Wenn ihr euch schon ein ungefähres Bild davon machen könnt, was euer Charakter können oder wie er sein soll, ist das für die Erstellung eures Charakters von entscheidendem Vorteil.

## Biografie bzw. Charakterfragen

 Falls ihr noch nicht so genau wisst, was ihr euch vorstellt, könnt ihr euch anhand der folgenden Fragen orientieren:

- Wo hat dein Charakter seine Kindheit verbracht? Was hat ihn dabei besonders geprägt?
- Wo hat dein Charakter seine Jugend verbracht?Welches Ereignis hat ihn/sie auf seinen/ihren Pfad geführt?
- Wovor hat er/sie am meisten Angst?
- Welche dunklen Geheimnisse hat er/sie?
- Wer sind seine/ihre Eltern und gibt es Geschwister? Hat er/sie eine eigene Familie?
- Wie geht er/sie auf neue Leute zu und welche Vorurteile hat er/sie?- Was ist sein/ihr wichtigster Besitz?
- Was sind seine/ihre großen Glaubenssätze, Philosophien oder Ideologien?
- Auf welches Ziel arbeitet er/sie hin?
- Was ist sein/ihr größter Fehler?
- Wenn er/sie mit all seinen/ihren Abenteuern fertig ist, wie will er/sie dann leben?

## Charakter in werten

### Attribute

Alle Attribute starten mit einem Wert von 2. Ihr könnt Attributspunkte 1:1 tauschen, aber nur 2 eurer Attribute dürfen Stufe 3 erreichen. Höher als Stufe 3 darf nicht gesteigert werden.

```admonish example title='Beispiel'
Ihr könnt Empathie auf Stufe 1 herabsetzen und dafür Geschick auf Stufe 3 steigern.
```

### Fertigkeiten

Fertigkeiten starten bei 0, was so viel heißt wie _ungelernt_. Ihr bekommt 10 Punkte für die Verteilung. Die maximale Stufe einer Fertigkeit ist bei der Erstellung auf 4 begrenzt.

### Aspekte & Stunts

Zu Beginn kannst du je 2 [Aspekte](/grundregelwerk/aspekte/index.md), [Kompetenzen](/grundregelwerk/kompetenzen/index.md) und  [Stunts](/grundregelwerk/stunts/index.md) aussuchen.

### Schicksalspunkte

Jeder Charakter startet mit einer Erholungsrate von 3.

#### Schwächen

Wenn die Spielleitung zugelassene Schwächen (Aspekte) entsprechend dem folgenden Tauschkurs annimmt, kann ein Charakter weitere Punkte erhalten. Ihr könnt maximal **zwei** Schwächen auswählen.

- _Eine_ Schwäche = _eine_ Kompetenz
- _Eine_ Schwäche = _drei_ Fertigkeitspunkte

#### während des Spiels auswählen

Wenn dir nicht sofort ein paar Ideen für deine [Aspekte](/grundregelwerk/aspekte/index.md) oder [Stunts](/grundregelwerk/stunts/index.md) kommen, kannst du sie auch später auf dem Charakterbogen eintragen. Wenn dir irgendwann etwas interessant erscheint, schreib es einfach auf. Das machst du normalerweise dann, wenn du dringend einen Aspekt oder Stunt brauchst, den du ausnutzen kannst, um etwas zu erreichen, oder weil dir in der Szene einfällt, wie man jetzt gut einen Aspekt reizen könnte.

Wenn du einen Stunt haben möchtest, aber den nötigen Fate-Punkt nicht hast, um ihn einzusetzen, schreib ihn trotzdem auf, ohne ihn einzusetzen.

Du musst dich nicht stressen, gleich die perfekte Formulierung zu finden. Nach der Sitzung kannst du die Aspekte oder Stunts noch so anpassen, wie du möchtest.

### Ressourcen

Dein Charakter startet mit 10 Punkten Ressourcen. Du kannst die Ressourcenpunkte vor Spielbeginn ausgeben, um besondere Ausrüstung oder Spezialaktionen zu erwerben, die nur durch den Einsatz von Ressourcen beschafft werden können.

### Ausrüstung

Bei Moirenico gibt es keine langen Ausrüstungslisten. Die Grundregel lautet: Dein Charakter hat alles dabei, was er braucht, um seine Fertigkeiten einzusetzen. Als Profi weiß er ohnehin besser als du, was er mitnehmen muss! Du musst aber festlegen, welche Waffen er dabeihat und was für eine Rüstung er trägt. Mehr Infos dazu gibt's unter [Waffen](/material/waffen/index.md) und [Rüstungen](/material/ruestungen/index.md). \
Wenn du zu Spielbeginn einen besonderen Gegenstand haben möchtest, der dir Boni auf deine Werte gibt, musst du dafür Ressourcen-Punkte ausgeben.

## Zusammenfassung

Hier eine kurze Zusammenfassung der wichtigsten Punkte für die Charaktererstellung:

- **Attribute:** 1:1 Tausch (max. 3)
- **Fertigkeiten:** 10 (max. 4)
- **Ressourcen:** 10
- **Aspekte:** 2
- **Kompetenzen:** 2
- **Stunts:** 2
- **Schwächen:** max 2
