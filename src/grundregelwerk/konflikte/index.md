# Konflikte

Sobald ein Konflikt entsteht, wird die Zeit angehalten und in Runden eingeteilt. Eine Runde entspricht etwa 6 Sekunden und dauert so lange, bis alle Akteure einmal an der Reihe waren. Zu Beginn des Kampfes haben alle Teilnehmer keine verfügbaren Aktionen. Erst wenn sie zum ersten Mal die Initiative ergreifen, erhalten sie 2 Aktionen und immer wenn sie erneut an der reihe sind. \
Ausgenommen davon sind Überraschungsrunden, wie z.B. ein Hinterhalt.

## Initiative

Die Initiative wird mit [Präzision](/grundregelwerk/attribute_fertigkeiten/index.md/#attribute) plus [Reaktions](/grundregelwerk/attribute_fertigkeiten/index.md/#fertigkeiten) d10 bestimmt. Hier werden die Werte zusammengezählt und nicht als Erfolge gewertet.

## Aktionen

- **Angreifen** mit einer Waffe
- [Kampfmanöver](/grundregelwerk/konflikte/kampf.md#kampfman%C3%B6ver)
- **Parieren** mit einer Waffe oder einem Gegenstand
- **Nachladen** einer Waffe
- **Benutzen** eines Gegenstands
- **Ausweichen** eines Angriffs
- **Zielen** mit einer Nahkampfwaffe oder beim Einzelschuss
- **Hinhocken** oder sich auf den Boden legen
- **Aufstehen**
- **Gehen** 6 Meter
- **Rennen** 6 Meter + [Körperkraft](/grundregelwerk/attribute_fertigkeiten/index.md)
- **Robben** 3 Meter, der Charakter muss in der Hocke sein
- **Rückzug**: Ihr könnt euch aus dem Kampf lösen, indem ihr eine Reaktions-Probe ablegt (eine Probe für jeden Gegner). Wenn ihr erfolgreich seid, entgeht ihr der Aktion eures Gegners und könnt euch soweit bewegen wie ihr Rennen könnt.
- **Sprechen**: Wenn ihr etwas sagen möchtet, könnt ihr zwei kurze Sätze oder einen längeren Satz äußern. Bitte beachtet, dass ihr etwa 3 Sekunden Zeit habt.

## Reaktionen

Voraussetzung für eine Reaktion ist, dass noch Aktionen zur Verfügung stehen und der Akteur wahrgenommen werden kann. \
Die Reaktion erfolgt direkt nach der Aktion des Akteurs, aber praktisch zeitgleich. Beispiele für Reaktionen:

- Parieren eines Angriffs
- Angriff auf einen Flüchtenden in Reichweite, falls nicht Rückzug genutzt wurde. Behindert nicht die Bewegung
- Fernkämpfer schießt in Angriffsreichweite eines Nahkämpfers
