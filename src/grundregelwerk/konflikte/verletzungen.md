<!-- markdownlint-disable no-duplicate-heading -->
# Verletzungen

Der Zustand deines Charakters wird durch Wunden, Stress und Konsequenzen dargestellt.

## Wunden & Stress

Jeder Charakter verfügt über zwei verschiedene Pools.

- **Wunden** umfasst jeglichen physischen Schaden.
- **Stress** umfasst jeglichen mentalen Schaden.

Charaktere haben mindestens jeweils **eins** plus ihren Vitalitätswert für Wunden und Empathie für Stress. Diese Wunden sind aufsteigend nummeriert, beginnend bei 1.

```admonish example title='Beispiel'
Bei einem Vitalitätswert von 3 und einem Empathiewert von 1 stehen euch also 4 Wunden und 2 Stress zur Verfügung. Ihr habt also einen Wundenpool von 1, 2, 3 und 4 und könnt damit insgesamt 10 Schaden absorbieren
```

Wenn man Schaden erleidet, muss man diesen auf die Wunden verteilen, um den Schaden ganz oder teilweise zu absorbieren. Wunden können so viel Schaden absorbieren, wie ihre Zahl angibt

```admonish example title='Beispiel'
BBei einem Angriff mit 4 Schaden kann man nun den Wundenplatz 4 nutzen oder 3 und 1.
```

Wenn ihr keinen kleineren Wundenplatz mehr zur Verfügung habt, müsst ihr den nächsthöheren nutzen und wenn ihr keine Wunden mehr einstecken könnt, erleidet ihr Konsequenzen

## Konsequenzen

Du kannst eine oder mehrere Konsequenzen in Kauf nehmen, um den Schaden zu mindern. Konsequenzen ähneln [Aspekten](/grundregelwerk/aspekte/index.md), haben jedoch ausschließlich negative Auswirkungen. Jeder Charakter hat drei Konsequenz-Stufen, die wie folgt gezählt werden:

- Leichte Konsequenz: Absorbiert 4 Punkte Schaden.
- Mittlere Konsequenz: Absorbiert 6 Punkte Schaden.
- Schwere Konsequenz: Absorbiert 8 Punkte Schaden.

### Extreme Konsequenzen

Zusätzlich zu der leichten milden, mittleren und schweren Konsequenzen gibt es noch eine letzte Möglichkeit, im Kampf zu Überleben: die **extreme Konsequenz**. Zwischen den Meilensteinen kann diese Option nur einmal genutzt werden.

Eine extreme Konsequenz absorbiert bis zu 10 Punkte Schaden, aber zu einem sehr hohen Preis - du musst einen deiner Aspekte durch die extreme Konsequenz ersetzen. D.h. sie ist so schwerwiegend, dass sie buchstäblich die Persönlichkeit verändert.

Im Gegensatz zu anderen Konsequenzen kannst kann sie nicht einfach Regeneriert werden - du bleibst bis zu deinem nächsten großen Meilenstein an ihr hängen. Danach kannst sie umbenannt werden, um zu verdeutlichen, dass du für die schlimmsten Folgen nicht mehr anfällig bist, kannst sie aber nicht gegen den alten Aspekt austauschen. Die Übernahme einer extremen Konsequenz ist eine *dauerhafte Charakterveränderung*; behandelt sie als solche.

## Sterben

Wenn ihr keine Konsequenzen mehr vergeben könnt, stirbt euer Charakter.

## Regeneration

Nach einem Konflikt, sobald eine Minute Zeit zum Durchatmen vorhanden ist, werden alle Wunden- und Stresskästchen wiederhergestellt.

Die Regeneration von Konsequenzen ist etwas schwieriger. Um eine Konsequenz zu entfernen, muss man sich zuerst davon erholen. Dafür sind zwei Schritte nötig:

- es muss eine Aktion ausgeführt werden, die die Erholung unterstützt
- es muss eine angemessene Zeitspanne abgewartet werden.

Die Art der Aktion hängt von der Art der Konsequenz ab: Bei körperlichen Verletzungen ist es beispielsweise eine medizinische Behandlung, bei psychischen Folgen könnte es eine Therapie oder eine entspannte Zeit mit Freunden sein. Die Schwierigkeit dieser Erholung hängt von der Schwere der Konsequenz ab:

- Leichte Konsequenz 1 Erfolg
- Mittlere Konsequenz 2 Erfolge
- schwere Konsequenz 3 Erfolge

Wenn man versucht, die Genesung selbst durchzuführen, wird die Schwierigkeit um eins erhöht. Die Umgebung muss ablenkungs- und spannungsfrei sein, um überhaupt eine Erholung durchführen zu können. Der Spielleiter hat das letzte Wort, ob dies der Fall ist. Nach einer erfolgreichen Erholungsaktion kann der Aspekt der Konsequenz umbenannt werden, um den Genesungsfortschritt zu verdeutlichen, z.B. von „Gebrochenes Bein“ zu „Im Gips stecken“. Dies zeigt an, dass die Genesung begonnen hat, belegt aber weiterhin den Konsequenzen Platz.

- Leichte Konsequenz: Verschwindet nach dem [Schlafen](/grundregelwerk/zeiteinteilung/lager.md#schlafen).
- Mittlere Konsequenz: Verschwindet am Ende der Spielsitzung.
- Schwere Konsequenz: Verschwindet am Ende des Szenarios.

```admonish info
Unabhängig davon, ob der Name geändert wird, sollte die Konsequenz markiert werden, um den Beginn der Genesung anzuzeigen.
```

### Heilung

In vielen Genres gibt es Mechanismen, die es Figuren ermöglichen, sich schnell von Verletzungen zu erholen, wie Heiltränke in Fantasy-Settings oder fortschrittliche Technologien in der Science-Fiction. Diese Mechanismen verhindern, dass Verletzungen die Effektivität eines Charakters drastisch beeinträchtigen. Eine Heilung ersetzt die Notwendigkeit einer Aktion und kann die Schwere einer Konsequenz verringern, z. B. eine schwere Folge in eine mittelschwere umwandeln, was die Erholungszeit verkürzt. Eine Heilung kann eine Konseqenz aber nur bis zur leichten Konsiquenz verringern.

## Benennung einer Konsequenz

### Leichte Konsequenz

erfordern keine sofortige medizinische Behandlung. Sie tun weh und sind vielleicht unangenehm, aber sie zwingen Sie nicht zu einer langen Bettruhe. Was die psychische Seite betrifft, so sind milde Folgen Ausdruck kleiner sozialer Entgleisungen oder Veränderungen in Ihren oberflächlichen Gefühlen.

Beispiele:

- Blaues Auge → Verblasster Bluterguss
- Geprellte Hand → Schwellung
- Erschöpft → Sich langsam erholend
- Verwirrt → Klarer Kopf kehrt zurück
- Launisch → Stimmung stabilisiert sich
- Vorübergehend erblindet → Sehkraft kehrt zurück

### Mittlere Konsequenz

stehen für schwerwiegende Beeinträchtigungen, die besondere Anstrengungen zur Genesung erfordern (einschließlich medizinischer Behandlung). Auf der mentalen Seite drücken sie Dinge wie Rufschädigung oder emotionale Probleme aus, die Sie nicht einfach mit einer Entschuldigung und einer guten Nachtruhe abtun können.

Beispiele:

- Tiefe Schnittwunde → Genäht und verbunden
- Verbrennung ersten Grades → Heilende Brandwunde
- Erschöpft → Körper regeneriert sich
- Betrunken → Kater setzt ein
- Verängstigt → Mut langsam wiederfinden

### Schwere Konsequenz

führen direkt in die Notaufnahme (oder was auch immer das Äquivalent in deinem Spiel ist) - sie sind extrem unangenehm und hindern dich daran, viele Dinge zu tun, und werden dich für eine Weile außer Gefecht setzen. Auf der mentalen Seite drücken sie Dinge wie ein schweres Trauma oder einen beziehungsändernden Schaden aus.

Beispiele:

- Verbrennungen zweiten Grades → Narbenbildung setzt ein
- Komplizierte Frakturen → Im Gips fixiert
- Heraushängende Eingeweide → Genäht und verbunden
- Lähmende Scham → Selbstwertgefühl langsam zurückgewonnen
- Traumabedingte Phobie → Therapie zeigt erste Wirkung
