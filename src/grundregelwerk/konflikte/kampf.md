# Kampf

Im folgenden Abschnitt findet ihr eine Beschreibung zum Kampf.

## Treffen

Um deinen Gegner im Kampf zu treffen, muss eine Nahkampf-Probe gelingen. Diese berechnet sich aus Attribut + Fertigkeit + Waffenschaden für die Würfelanzahl und Attribut für die geltenden Würfel.

### Fehschlag beim Fernkampfanriff

Wird in einen Nahkampf geschossen und die Probe gelingt nicht, wird ein weiteres Mal ein Angriff gewürfelt und dieser trifft ein zufälliges Wesen in dem Kampfgemenge, dies kann auch das anvisierte Wesen sein.

### Schaden

Wenn ein Treffer erzielt wird, wird [Schaden](/grundregelwerk/konflikte/verletzungen.md) in Höhe der Erfolge abzüglich des Rüstungswerts verursacht. Der Rüstungswert(RW) kann vermindert werden wenn die Waffe Rüstungsdurchdringung(RD) besitzt.

```admonish example title='Beispiel'
Ein Schwerthieb mit 9, 3, 4, 12 geltenden Würfeln führt zu dem Ergebnis von 2 Treffern. Trägt der Gegner eine leichte Rüstung, würde es 2 Schaden bedeuten.
```

### Parieren

Um einen Angriff zu parieren, muss eine konkurrierende [Nahkampf](/grundregelwerk/attribute_fertigkeiten/index.md#fertigkeiten)-Probe gelingen. Die Erfolge der Parade werden dann gegen die Erfolge des Angriffes gerechnet.

#### Kritische Treffer

Bei einem kritischen Treffer wird der Rüstungswert des getroffenen Charakters um 1 Punkt verringert, vorausgesetzt, der Rüstungswert stammt von der Ausrüstung. Nach der Kampfszene kann die Rüstung einfach wieder repariert werden. Schaden wird immer vor dem Rüstungsabzug berechnet. Falls keine Rüstung mehr vorhanden ist, die abgezogen werden kann, werden die kritischen Treffer als Schaden verrechnet.

```admonish example title='Beispiel'
Ein Schwerthieb mit 9, 6, 2, 19 geltenden Würfeln führt zu dem Ergebnis von 2 kritische Treffer. Trägt der Gegner eine schwere Rüstung, würde es 1 Schaden erleiden und 2 Rüstungspunkte verlieren.
```

#### Flächenangriff

Der zugefügte Schaden wird auf alle Betroffenen aufgeteilt.

## Coup de grâce

Wenn dein Gegner bewusstlos, schlafend oder sterbend ist, kannst du eine Angriffswurf-Probe mit Vorteil ablegen. Jeder Treffer gilt dann als kritischer Treffer. Falls der Angriff fehlschlägt, kann es sein, dass das Opfer aufwacht.

## Kampfmanöver

Kampfmanöver werden wie Waffen gehandhabt, da sie meist die Angriffs- oder Schadenswerte verändern. Folgend ein paar Beispiele:

### Sturmangriff

Einen Sturmangriff kann man nur in gerader Linie auf den Gegner machen, Hindernisse verhindern diesen. Der Waffenschaden verdoppelt sich, dafür verliert ihr die Möglichkeit euch für diese Runde zu verteidigen.

### Zielen

Bei Nahkampfwaffen und beim Einzelschuss ist es möglich, mit der Waffe zu zielen. Für jede aufgewendete Aktion wird der Bereich für Erfolge um 1 verringert.
Wird der Charakter während des Zielens getroffen, so wird der kritische Trefferbereich zurückgesetzt.

```admonish example title='Beispiel'
Bei einem mal ziehlen wären die Erfolgsgerade:

|                 | MS  | Erfolge                   |
| --------------- | --- | ------------------------- |
| normaler Erfolg | 8   | einen zusätzlichen Erfolg |
| großer Erfolg   | 18  | zwei zusätzliche Erfolge  |
| epischer Erfolg | 28  | drei zusätzliche Erfolge  |
```

### Gezielter Schlag/Schuss

Durch einen gezielten Angriff können besondere Effekte erzielt werden. Die Spielleitung bestimmt je nach Situation den spezifischen Effekt. Optional kann der gezielte Schuss auch den Schaden erhöhen. \
Je nach Schwere des Effekts müssen eine bestimmte Anzahl von Erfolgen erzielt werden. Diese Effekte funktionieren nur, wenn die angezielte Stelle ungeschützt ist.

```admonish example title='Beispiel'
| Körperteil | Erfolge | Auswirkungen                                                                                                                         |
| ---------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| Arm        | 2       | Nachteil beim benutzen des Arms                                                                                                      |
| Bein       | 2       | Halbiert die Laufgeschwindigkeit                                                                                                     |
| Hand       | 3       | Kann die Hand nicht mehr benutzen, lässt z.B die Waffe fallen.                                                                       |
| Kopf       | 4       | Bewusstlosigkeit wenn keine [Zähigkeit](/grundregelwerk/aktionen/rettungswuerfe.md#z%C3%A4higkeit)s-Probe bestanden wird.            |
| Auge       | 5       | Nachteil auf Wahrnehnung bzw. Blindheit wenn kein Auge mehr vorhanden.                                                               |
| Herz       | 6       | Sofortiger Tod.                                                                                                                      |
```

### Burst

Dem **Angriffswurf** werden **2 Würfel** hinzugefügt. Der Angriff kann **nicht pariert** werden, und dem Angriff kann **nicht ausgewichen** werden. Der Angriff kann **keine kritischen Treffer** verursachen, und für den Angriff kann der Charakter **nicht zielen**.

### Nicht tödlicher Angriff

Ein Angreifer kann sich auch für einen nicht tödlichen Angriff entscheiden, sofern dies mit seiner Waffe grundsätzlich möglich ist. Dies muss aber vor dem Angriff angesagt werden.

### Konter

Nach dem Parieren könnt ihr dem Gegner Schaden zufügen, wenn ihr mehr Erfolge erzielt als der Angreifer.
