# Kompetenzen

Kompetenzen repräsentieren die verschiedenen Fähigkeiten und Wissensgebiete eines Charakters. Sie spiegeln die Fähigkeiten, Erfahrungen und Talente der Figur wider und prägen maßgeblich ihre Handlungsmöglichkeiten in der Spielwelt. Kompetenzen sind in unterschiedliche Bereiche gegliedert und umfassen körperliche, geistige und soziale Fähigkeiten, die es den Spielern ermöglichen, gezielt Schwerpunkte zu setzen und dadurch spezifische Stärken oder Schwächen ihrer Figur auszubauen.

Die Auswahl und Weiterentwicklung der Kompetenzen einer Spielfigur beeinflussen den Verlauf des Spiels und die Interaktionen mit der Welt von Moirenico. Abhängig von ihren Kompetenzen kann eine Figur Herausforderungen unterschiedlich bewältigen, Konflikte auf ihre eigene Weise lösen und ihre Umgebung aktiv beeinflussen. Kompetenzen verleihen dem Charakter Profil und Tiefe.
