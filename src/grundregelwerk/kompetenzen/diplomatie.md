# Diplomatie

Die Diplomatie-Kompetenz umfasst soziale Fähigkeiten, um die Interaktion mit anderen zu beeinflussen, sei es durch Überzeugung, Einschüchterung oder die Kunst der Präsentation. Sie ist nützlich für Situationen, in denen das Verhandeln, das Vermitteln von Beziehungen oder das Erzielen eines bestimmten Ergebnisses im Vordergrund steht.

## Überreden

Überzeugen ist die Fähigkeit, andere mit Argumenten und Charme zu beeinflussen und von einer bestimmten Sichtweise oder Handlung zu überzeugen. Diese Fähigkeit erfordert Empathie, Kommunikationsgeschick und die Fähigkeit, sich in die Position des Gegenübers zu versetzen. Sie ist besonders nützlich, um Verhandlungen zu führen oder Bündnisse zu schließen.

## Einschüchtern

Die Fähigkeit, durch gezielte Drohungen oder eine starke, furchteinflößende Präsenz andere zu beeinflussen. Einschüchtern wird oft eingesetzt, um Widerstand zu brechen oder eine sofortige Reaktion zu erzwingen. Charaktere, die diese Fähigkeit gut beherrschen, können Gegner oft ohne physische Gewalt zur Aufgabe bewegen oder ihren Willen durchsetzen.

## Performance

Performance ist die Fähigkeit, durch eine gekonnte Darbietung Aufmerksamkeit zu gewinnen und andere zu beeinflussen oder zu beeindrucken. Sie umfasst alle Arten von Aufführungen wie Singen, Tanzen, Reden oder das Vortragen einer Geschichte. Diese Fähigkeit ist nützlich, um eine Menge zu begeistern, bestimmte Botschaften emotional zu übermitteln oder eine dramatische Wirkung zu erzielen.
