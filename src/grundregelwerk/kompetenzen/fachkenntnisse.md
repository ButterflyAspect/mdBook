# Fachkenntnisse

Die Fachkenntnisse umfassen verschiedene intellektuelle und handwerkliche Fähigkeiten, die es dem Charakter ermöglichen, Wissen anzuwenden und praktische Aufgaben zu bewältigen. Diese Kompetenz ist für Situationen nützlich, die tiefere Kenntnisse oder spezielle Fertigkeiten erfordern. Zu den folgenden, jedoch nicht abschließenden Fachkentnissen zählen:

## Geschichte/Legende/Sage

Diese Fähigkeit umfasst ein fundiertes Wissen über historische Ereignisse, alte Legenden und Sagen. Charaktere mit dieser Fachkenntnis können Geschichten der Vergangenheit interpretieren, Zusammenhänge erkennen und wichtige Informationen über kulturelle Mythen und bedeutende Persönlichkeiten der Geschichte bereitstellen. Diese Fähigkeit ist nützlich, um alte Texte zu verstehen oder die Bedeutung von Symbolen und Artefakten zu entschlüsseln.

## Alchemie/Kochen

Alchemie und Kochen beinhalten die Fähigkeit, verschiedene Zutaten und Stoffe zu kombinieren, um Tränke, Heilmittel oder Speisen herzustellen. Während Alchemie die Herstellung von magischen oder heilenden Mixturen ermöglicht, ist Kochen für die Zubereitung nahrhafter Mahlzeiten wichtig. Charaktere mit diesen Fähigkeiten können verschiedene Materialien effektiv verarbeiten und nutzen.

## Verarbeitung (zu Spezifizieren)

Die Verarbeitungsfähigkeiten umfassen das handwerkliche Geschick, um Materialien wie Holz, Metall oder Leder zu verarbeiten. Dazu gehören Tätigkeiten wie Schmieden, Nähen und Schnitzen, die für die Herstellung und Reparatur von Gegenständen erforderlich sind. Diese Kompetenz ist wertvoll, um Werkzeuge, Waffen, Rüstungen oder andere Gegenstände anzufertigen und instand zu halten.

## Medizin

Die medizinische Fachkenntnis erlaubt es einem Charakter, Verletzungen zu versorgen und Krankheiten zu behandeln. Sie umfasst die Kenntnisse der Anatomie, der Heilpflanzen und der Behandlungstechniken, um Wunden zu versorgen oder Gifte zu neutralisieren. Diese Fähigkeit ist besonders wichtig, um Verwundete zu stabilisieren oder lebensbedrohliche Zustände zu verhindern.
