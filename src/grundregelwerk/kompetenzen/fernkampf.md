# Fernkampf

Die Fernkampf-Kompetenz umfasst das Wissen und die Fähigkeiten, um verschiedene Distanzwaffen effektiv einzusetzen. Jede Waffenart erfordert spezifische Techniken, um präzise, kraftvolle Angriffe über eine größere Distanz auszuführen.

## Bogen

Der Bogen ist eine Fernkampfwaffe, die Geschicklichkeit und präzises Zielen erfordert. Bogenschützen müssen die richtige Spannung der Sehne, die Positionierung des Pfeils und den Wind berücksichtigen, um das Ziel zu treffen.

## Armbrust

Die Armbrust ist eine kraftvolle Fernkampfwaffe, die aufgrund ihres Mechanismus höhere Durchschlagskraft und Präzision auf größere Distanzen bietet. Im Gegensatz zum Bogen erfordert sie weniger körperliche Kraft zum Spannen, benötigt jedoch mehr Zeit zum Nachladen.

## Wurfwaffe

Wurfwaffen sind für den schnellen Einsatz über kurze bis mittlere Distanzen gedacht. Zu den Wurfwaffen gehören Messer, Speere und Äxte, die für schnelle, präzise Würfe optimiert sind.
