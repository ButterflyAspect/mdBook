# Nahkampf

Die Nahkampf-Kompetenz umfasst verschiedene Fähigkeiten und Fertigkeiten, die im Kampf auf kurze Distanz eingesetzt werden können. Die Handhabung verschiedener Waffentypen basieren auf ihrem Gewicht und Schwerpunkt, und die Wahl zwischen unterschiedlichen Waffenkategorien, die den Stil und die Art des Angriffs beeinflussen.

## Waffenlos

Die grundlegende Nahkampffähigkeit, die ohne Waffen ausgeführt wird. Sie umfasst Techniken des direkten, körperlichen Kampfes wie Schläge, Tritte und Würfe.

## Peitsche

Die grundlegende Nahkampffähigkeit, die mit einer Peitsche ausgeführt wird. Sie umfasst Techniken mit einer Peitsche für Distanzangriffe innerhalb der Nahkampf-Reichweite.

## Schwere

Die Schwere einer Waffe beschreibt das Gewicht und den Schwerpunkt, der die Handhabung und den Kampfstil stark beeinflusst. Es gibt drei Schweregrade:

1. **Leicht (Schwerpunkt am Griff):**
  Diese Waffen sind gut ausbalanciert für schnelle, präzise Angriffe und ein hohes Maß an Kontrolle. Der Schwerpunkt liegt nahe am Griff, was schnelle Richtungswechsel und flexible Angriffe erlaubt. Ein typisches Beispiel ist ein Dolch, der schnelle, gezielte Stöße ermöglicht.

2. **Ausgewogen (Schwerpunkt mittig):**
  Diese Waffen haben ein gleichmäßiges Gewicht, was eine Mischung aus Kontrolle und Schlagkraft ermöglicht. Sie sind vielseitig einsetzbar und eignen sich für eine Vielzahl von Angriffstechniken. Ein ausgewogenes Schwert wäre ein gutes Beispiel, das sowohl für Hiebe als auch für Stöße geeignet ist.

3. **Schwer (Schwerpunkt am Kopf):**
  Diese Waffen haben ihren Schwerpunkt am Kopf, was ihnen eine hohe Durchschlagskraft verleiht. Der Nachteil ist, dass sie langsamer und schwerer zu kontrollieren sind, jedoch besonders im Durchbrechen von Verteidigungen oder im Zufügen schwerer Schäden effektiv sind. Ein typischer Vertreter dieser Kategorie ist der Kriegshammer.

## Kategorie

Die Kategorie legt fest, wie die Waffe geführt wird. Sie unterteilt sich in vier verschiedene Stile:

1. **Einhand:**
  Einhandwaffen können mit einer Hand geführt werden, wodurch die zweite Hand frei bleibt (zum Beispiel für eine Fackel oder zur Balance). Diese Waffen bieten Beweglichkeit und Flexibilität.

2. **Zweihand:**
  Zweihandwaffen werden mit beiden Händen geführt und sind meist länger und schwerer, was größere Wucht und Reichweite ermöglicht.

3. **Zwei-Waffen kampf:**
  Zwei einhandwaffen in die jeweils in einer Hand geführt werden.

4. **Schildkampf:**
  Beim Schildkampf wird ein Schild in Kombination mit einer Waffe geführt, wodurch sowohl Angriff als auch Verteidigung nahtlos integriert werden können.

## Kombinationen aus Schwere und Kategorie

Durch die Kombination von Schwere und Kategorie entstehen unterschiedliche Waffentypen mit spezifischen Eigenschaften. So ergibt sich ein breites Spektrum an Waffenstilen, die je nach Vorlieben und Strategie des Kämpfers eingesetzt werden können. Beispiele hierfür sind:

- **Leicht Einhand:** Ein Dolch, der für schnelle und präzise Angriffe eingesetzt wird.
- **Ausgewogen Zweihand:** Ein Langschwert, das eine Balance zwischen Kraft und Geschwindigkeit bietet.
- **Schwer Zweihand:** Ein Kriegshammer, der durch seine Wucht schwere Rüstungen durchbrechen kann.
- **Leicht Schildkampf:** Ein Kurzschwert, das in Kombination mit einem Schild geführt wird, für eine ausgeglichene Mischung aus Verteidigung und Angriff.
