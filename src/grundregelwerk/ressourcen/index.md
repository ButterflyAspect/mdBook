# Ressourcen

## Ressourcen-Monitor

Bei Moirenico werden keine einzelnen Münzen gezählt, sondern Ressourcen verwendet. Ressourcen sind eine abstrakte Größe für Goldstücke, Kontoguthaben, Creditsticks, Tauschware und geldwerte Gefallen, die man dir schuldet oder, schlimmer noch, du jemanden schuldest.
Ich empfehle, einen Ressourcenpunkt im mittelalterlichen Setting als Goldstück zu bezeichnen. Das könnt ihr auch für andere Settings so handhaben.

## Ressourcen gewinnen

Du bekommst Ressourcen für das erfolgreiche Abschließen deiner Aufgaben. Nach Abzug deiner laufenden Kosten bleibt nach dem Auftrag grundsätzlich mindestens ein Punkt Ressourcen pro Teammitglied übrig. Und gelegentlich wirst du auf richtig wertvolle Beute stoßen, die sich auch zu Geld machen lässt. Nur richtig außergewöhnliche Beute lässt sich auf diese Weise verkaufen, zum Beispiel das Schwert oder die Maschinenpistole des Wachmanns.

## Ressourcen verwenden

Deine Ressourcen kannst du auf verschiedene Art und Weise ausgeben:

- Du kannst einen Punkt Ressourcen einsetzen, um einen Schub zu erzeugen, der darstellt, dass du mit viel Geld um dich wirfst. Das ist zum Beispiel hilfreich, wenn Du kannst Spezialausrüstung besorgen (Vorteil auf die entsprechende Probe) oder dem Wachmann eine Flasche teuren Schnaps in die Hand drücken, bevor du ihn um einen Gefallen bittest.
- Besondere Ausrüstungsgegenstände, die bestimmte Werte haben, kosten in der Regel Ressourcenpunkte. Mehr dazu findest du im „Material” unter [Waffen](/material/waffen/index.md) und [Rüstungen](/material/ruestungen/index.md).
- Manchmal verlangt die Spielleitung, dass du einen Ressourcenpunkt einsetzt. Wenn du zum Beispiel eine Söldnergruppe anheuerst, die dich für ein paar Tage begleitet, kostet das die Gruppe natürlich etwas.
