# Verbesserungen

Rüstungen können durch bestimmte Materialien verändert werden, um gegen bestimmte Schadensarten besser gewappnet zu sein. Die Rüstungsasse beschreibt die Stufe, auf die das Material angewendet werden kann, bei einem + oder - kann es auch auf höhere bzw niedrigere Rüstungen angewendet werden. \

| **Material**    | **Rüstungsasse**   | **Modifikation**                        | **Kosten** |
| --------------- | ------------------ | --------------------------------------- | ---------- |
| Zwergenstahl    | Schwere Rüstung    | RW +1                                   | \*3        |
| Adamantit       | Mittlere Rüstung + | halbe Rüstung gegen Magie               | \*5        |
| Schattenseide   | Mittlere Rüstung - | Vorteil auf List                        | -          |
| Mithril         | Mittlere Rüstung + | RW +2                                   | \*6        |
| Drachenleder    | Mittlere Rüstung - | halbiert Elementschaden(je nach Drache) | -          |
| Drachenschuppen | Mittlere Rüstung + | halbiert Elementschaden(je nach Drache) | -          |
| Adamantium      | Schwere Rüstung    | gesamte Rüstung gegen Magie             | \*6        |
