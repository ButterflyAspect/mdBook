# Rüstungen

## Mittelalter (ca. 5. bis 15. Jahrhundert)

| **Stufe**        | **Information**                           | **Attribut** | **Rüstungswert** | **Kosten** |
| ---------------- | ----------------------------------------- | ------------ | ---------------- | ---------- |
| Kleidung         | Treffer verursacht eine zusätzliche Wunde | 0            | 0                | 0          |
| Leichte Rüstung  | z.B. Lederrüstung                         | 1            | 0                | 2          |
| Mittlere Rüstung | z.B. Beschlagene Rüstung oder Kettenhemd  | 2            | 1                | 3          |
| Schwere Rüstung  | z.B. Platten-/Ritterrüstung               | 3            | 2                | 4          |
| Buckler          | wie leichte Rüstung                       | 1            | 0                | 1          |
| Rundschild       | wie mittlere Rüstung                      | 2            | 1                | 2          |
| Turmschild       | wie schwere Rüstung, eine Parade frei     | 3            | 1                | 3          |

Schwere Rüstungen und das Turmschild geben euch einen Nachteil auf List & Bewegung
