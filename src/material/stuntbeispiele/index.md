# Stuntsbeispiele

## Spieler

Im Folgenden findest du einige Beispiele für Stunts, die dein Charakter besitzen kann. Stunts sind üblicherweise an eine Fertigkeit gekoppelt. Du kannst auch Stunts für Fertigkeiten wählen, die du nicht einmal besitzt, allerdings solltest du dir gut überlegen, ob das sinnvoll ist.

### Ich bin immer der Erste

Du kannst einen Schicksal-Punkt ausgeben, um als erster in einer Runde zu handeln, ohne Rücksicht auf die Initiativ Reihenfolge zu nehmen. Wenn mehrere Beteiligte diesen Stunt haben, agiert ihr in der Reihenfolge der normalen Initiative, aber noch vor allen anderen. Wenn du in einer Runde noch nicht gehandelt hast, kannst du einen Schicksal-Punkt zahlen, um sofort und außerhalb der normalen Initiative zu handeln.
Dies kann nur zwischen den Aktionen von anderen Charakteren stattfinden und nicht als eine Unterbrechung (d. h. wenn du einen Schicksal-Punkt ausgibst und diesen Stunt benutzt, aber ein anderer Charakter noch am Zug ist, musst du warten, bis er mit seiner Aktion fertig ist). Das geht aber nur, wenn du in dieser Runde noch nicht gehandelt hast.

### Allzeit bereit

*Voraussetzung: Ich bin immer der Erste!* <!-- markdownlint-disable-line no-emphasis-as-heading -->

Die Sinne deines Charakters sind so geschult, dass du auf Veränderungen wesentlich schneller reagierst. Für die Initiative bekommst du auf die Fertigkeit Wahrnehmung einen Vorteil. Wenn du auf einen Gegner mit der gleichen Initiative triffst, darfst du durch den Stunt zuerst handeln.

### Anrempeln und Zugreifen

Dein Charakter ist darin geübt, jemanden anzurempeln und gleichzeitig in seine Taschen zu greifen. Du kannst einen Schicksal-Punkt ausgeben, um eine List-Probe als freie Aktion in einem Kampf durchzuführen und eine Brieftasche, einen Gegenstand oder ähnliches zu entwenden.

### Das ist die letzte Kugel

Die letzte Kugel im Lauf hat etwas Magisches. Mit diesem Stunt kannst du festlegen, dass dein Charakter nur noch eine Kugel in der Waffe hat. Damit kannst du jedoch einen einzelnen Schusswaffenangriff mit einem Vorteil ausführen und richteste doppelten Waffenschaden an.
Naturgemäß ist dies der unwiderruflich letzte Schuss des Charakters, d. h. dein Magazin ist leer, und du hast keine Ersatz oder versteckte Waffe bei dir. Um deine Schusswaffen-Fertigkeit wieder nutzen zu können, musst du dir neue Munition oder eine neue Waffe besorgen.

### Defensive Fahrweise

Du bist darin geübt, dein Fahrzeug heil aus dem Schlamassel zu bringen. Wenn du eine Probe für eine Verfolgungsjagd durchführst, wird der Schwierigkeitsgrad für dich um eine Stufe gesenkt.

### Der Himmel ist meine Westentasche

Der Himmel ist wie eine vertraute Landkarte für deinen Charakter. Wenn keine wirklich ungewöhnlichen Umstände herrschen, kannst du dich beim Fliegen nicht verirren. Wenn doch etwas Außergewöhnliches passiert, wird der Schwierigkeitsgrad für Fliegen-Proben niemals um mehr als 2 Stufen erhöht.

### Dickes Fell

Die meisten Charaktere mit einer guten Konstitution haben sich unter Kontrolle, wenn es kritisch wird – für dich jedoch ist Selbstkontrolle zur zweiten Natur geworden. Auch unter widrigsten Umständen oder bei gröbsten Beleidigungen zuckst du nicht einmal mit der Wimper. Außerhalb eines Kampfes kann dich nur sehr wenig erschüttern.
Du bekommst zusätzlich 2 Rüstung.

### Ringkämpfer

Dein Charakter ist ein ausgebildeter Ringkämpfer. Du erhälst einen Vorteil auf Nahkampf.

### Fangen

Wenn du dich erfolgreich gegen einen geworfenen Gegenstand verteidigst fängst du den Gegenstand auf. Du musst allerdings eine freie Hand haben, und der Gegenstand muss auf dich gezielt gewesen sein. Du kannst keine Gegenstände fangen, die dafür nicht geeignet sind (wie Sägeblätter, Kühlschränke oder Wackelpudding).

### Firmengewinne

Du leitest eine profitable Organisation oder ein Unternehmen. Gib einen Schicksal-Punkt aus, um deine Ressourcen-Fertigkeit temporär bis auf die maximale anzuheben. Es ist nicht wichtig, ob du in deinem Unternehmen anwesend bist oder nicht.

### Gefahrensinn

Dein Charakter besitzt eine erhöhte Wachsamkeit gegenüber Hinterhalten und anderen bösen Überraschungen. Vielleicht hat er ja übernatürliche oder einfach gut geschulte Sinne. Immer wenn du in einen Hinterhalt gerätst, kannst du dich Verteidigen.

### Gute Kontakte

Du kannst einmal pro Sitzung einen hilfreichen Verbündeten genau am richtigen Ort finden.

### MacGyver

Du kannst in den unmöglichsten Situationen aus handelsüblichen Gegenständen etwas hilfreiches für dich oder deine Gruppe herstellen.

### Hau-ruck-Reparatur

*Voraussetzung: MacGyver* <!-- markdownlint-disable-line no-emphasis-as-heading -->

Manchmal kann ein gezielter Schlag Wunder wirken und eine Reihe von Reparaturen ersetzen. Du musst einen Schicksal-Punkt bezahlen, um diese Fähigkeit zu benutzen und eine Handwerk-Probe durchführen (mit einer einfachen Schwierigkeit). Anschließend prügelst du auf einen Gegenstand oder eine Apparatur ein, und sie läuft wieder wie geschmiert – egal wie hoch der Schwierigkeitsgrad ist, um sie unter normalen Bedingungen zu reparieren.

### International

Die vollendete Etikette deines Charakters lässt ihn überall sicher und wortgewandt auftreten. Du erleidest niemals einen Nachteil oder eine Erhöhung des Schwierigkeitsgrades, wenn du in fremder Umgebung unterwegs bist. Dies ermöglicht dir, dich durch unbekannte Örtlichkeiten zu manövrieren, dich lokalen Sitten und Gebräuchen anzupassen, und Ausrutscher mit einem Lächeln und einer nonchalanten Geste zu überspielen.

### Keine Bewegung

Jeder Charakter kann die Fertigkeit Diplomatie nutzen, doch wann immer du jemanden versuchst aufzuhalten klappt es immer. Wenn du einen Schicksal Punkt ausgibst kannst du dafür sorgen das ein Gegner in seiner Aktion inne hält und diese Kampfrunde aussetzt.

### Leise

Mit deinem Talent Heimlichkeit kann auf andere anleiten, die sich in deiner Nähe befinden. Solange die gesamte Gruppe bei dir ist und deinen leisen Befehlen folgt, kannst du eine List-Probe für die ganze Gruppe durchführen. Falls jemand eure Gruppe verlässt, verliert dieser sofort den Vorteil.

### Lippenlesen

Dein Charakter kann die Fertigkeit Wahrnehmung benutzen, um Gesprächen zu folgen, die er nur beobachten kann.

### Meister der Schlösser

Solange du einen Gegenstand bei dir trägst, den man in irgendeiner Form als Werkzeug zum Schlösser öffnen verwenden kann, erleidest du niemals einen Malus auf Einbrechen-Proben, weil dir ordentliche Werkzeuge fehlen. Und wenn du die entsprechenden Werkzeuge besitzt, kannst du jedes normale Schloss um eine Schwierigkeitsstufe leichter knacken.

### Menschenkenner

Du verstehst die Strömungen einer sozialen Situation so gut, dass du schon im Vorhinein sagen kannst, was als Nächstes passieren wird.
Zu Beginn einer beliebigen sozialen Interaktion, noch vor der üblichen Initiativ Reihenfolge, kannst du einen Schicksal-Punkt ausgeben, um Mimik und Gestik seines Gegenübers so intensiv zu studieren, dass du dessen unmittelbare Handlung vorhersagen kann. Dies ist eine freie Aktion, nach der du wieder in der normalen Initiativ Reihenfolge handelst.

### Notausgang

Du brauchst nicht mehr als eine kurze Ablenkung, um dich von einer Szene abzusetzen.
Wenn du nicht im Zentrum eines Konflikts stehst, kannst du eine List-Probe mit der Schwierigkeit des höchsten Wahrnehmung-Wertes aller Anwesenden durchführen. Bei einem Erfolg bist du einfach weg, wenn das nächste Mal jemand nach dir schaut oder mit dir sprechen will.

### Ort des Verbrechens

Du besitzt ein großartiges visuelles Gedächtnis. Immer, wenn du einen Ort wieder aufsuchst, an dem du Wahrnehmung benutzt hast, kannst du eine neue Wahrnehmung-Probe ablegen. Wenn du Erfolg hast, siehst du innerhalb weniger Sekunden, was sich an dem Ort seit dem letzten Mal geändert hat.

### Ruhige Hand

Eine unruhige Hand kann kritisch sein, wenn es haarig zugeht. Die Hände deines Charakters zittern und zucken niemals. Dein Charakter kann Schwierigkeitserhöhungen durch die Umwelt ignorieren, wenn er präzise Arbeiten mit den Händen durchführt.

### Sanitäter

Dein Charakter ist in der medizinischen Feldunterstützung talentiert. Für einen Schicksalpunkt kannst du jeden verletzen Stabilisieren und am leben erhalten.

### Schmutzige Tricks

Dein Charakter besitzt ein Talent für unfaire Kampftechniken und ist darin geübt, schmutzige Tricks anzuwenden, um die Überhand zu gewinnen. Du nutzt die Schwachstellen deines Gegners aus, um ihn dort zu treffen, wo es richtig weh tut. Du erhältst einen Vorteil in einer Kampffertigkeit.

### Schuss auf Distanz

Du kannst weiter schießen als jeder andere Schütze, warum auch immer. In deinen ruhigen Händen wird jeder Reichweitenschwierigkeit um eins reduziert.

### Sechster Sinn

Du bist offen gegenüber dem Fremden und Übernatürlichen. Normalerweise musst du eine Wahrnehmung-Probe machen um das Übernatürliche oder Fremde an einem Ort zu spüren. Mit diesem Stunt kannst du deine Mysterien-Fertigkeit nutzen, um einen Einblick in die okkulten Gegebenheiten eines Ortes zu erlangen.

### Worte im Wind

*Voraussetzung: Sechster Sinn* <!-- markdownlint-disable-line no-emphasis-as-heading -->

Manche Dinge enthalten Muster, die nicht einmal den Weisen sofort ersichtlich sind. Du jedoch weißt, wohin du blicken und wie du hören musst, um diese Muster zu erkennen.
Einmal pro Spielabend kannst du eine Mysterien-Probe durchführen, um aus den magischen Mustern ein Omen zu lesen.
Der SL entscheidet anhand des Ergebnisses der Probe, wie obskur die Informationen des Omens sind – es kann sich um ziemlich unverständliche Fetzen eines Rätsels handeln, oder um eine scheinbar normale Zeitungsmeldung über ein Schiff, das heute Nacht in den Hafen segeln wird.

### Schnell wie der Wind

Du kannst einmal pro Sitzung einfach da auftauchen, wo du möchte. Dieser Ort muss zu Fuß erreichbar sein können.

### Spezialfahrzeug

Du besitzt ein spezielles Fahrzeug abseits deiner Ressourcen-Fertigkeit.
Zusätzlich besitzt dein Fahrzeug 2 Extras. Du kannst einmal pro Spielabend einen Schicksal-Punkt ausgeben um diese zu aktivieren (wie einen Ölspur oder Krähenfusstreuer).

### Spinnenmensch

Du kannst Oberflächen emporklettern, die sonst niemand hinaufkommt, z. B. eine glitschige Glasfassade im strömenden Regen. Hierfür bekommst du keinen Malus auf Bewegungs-Proben. Wenn du zusätzlich einen Schicksal-Punkt ausgibst, kannst du alle Mali aufgrund von Umgebung oder Oberflächenbeschaffenheit ignorieren.

### Sprachbegabt

Normalerweise spricht dein Charakter eine Anzahl an Sprachen in Höhe seiner Fachkenntnisse-Fertigkeit, bei entsprechender Kompetenz. Mit dem Stunt kann dein Charakter doppelt so viele Sprachen erlernen.

### Unterschwellige Drohung

Du wirkst einfach immer bedrohlich, egal, ob du in einer Situation bist, in der du jemandem schaden kannst oder nicht. Selbst gefesselt oder hinter Gefängnisgittern sieht man dir deutlich an, dass du zu unglaublichen Gräueltaten fähig bist.
Egal, in welcher Position du dich befindest, du kannst Diplomatie immer benutzen.

### Wandelnde Bibliothek

Du hast verdammt viel gelesen und bist in der Lage, dich selbst an die kleinsten Details irgendwelcher völlig obskuren Werke zu erinnern. Es ist so, als hättest du eine Bücherei mit einer Qualität in Höhe deiner Fachkenntnisse-Fertigkeit zur Hand. Dadurch kannst du alle Fragen mit einer Fachkenntnisse-Probe beantworten und brauchst dazu nichts weiter als dein Gedächtnis und etwas Zeit zum Nachdenken.

### Großtat

Du kannst die Schwierigkeit deiner Probe erhöhen um eine Großtat zu vollbringen. Du kannst dadurch filmreife Aktionen ausführen. Du könntest z.B. über einen Kronleuchter zu deinem Widersacher schwingst oder an einem Wandteppich heruntergleiten lassen um ihm zu begegnet. Bei erfolgreicher Großtat gewährt dir die Spielleitung boni auf deine Aktionen.

### Schmerz ignorieren

Einmal pro Kampf kann der Charakter auf Resistenz würfeln. Bei Erfolg wird der Schaden einer Angriffsquelle in dieser Aktion vollständig verhindert. Diese Aktion erfordert keine zusätzliche Aktion.

### Arkane Erholung

Einmal am Tag kannst du außerhalb einer Ruhephase deine Verbindung zur Magie erneuern und dein Mana auffüllen. Dazu musst du etwa eine Stunde Ruhe finden, um dich auf den Fluss der Magie zu konzentrieren.

Würfle auf deinen Magiekunde-Wert und fülle mit der Hälfte der Erfolge deine Mana auf.

### Looter

Bei einem Untersuchenwurf kann der Charakter durch einen kritischen Erfolg besonders interessante Dinge entdecken.

### Besondere Wachsamkeit

Zu Beginn eines Kampfes erhält der Charakter eine zusätzliche Aktion, die jedoch nur für Reaktionen verwendet werden kann. Sobald der Charakter in der ersten Kampfrunde an der Reihe ist, ersetzen seine Aktionen diese zusätzliche Aktion.

### Gut ausgerüstet

Der Charakter kann einen Gegenstand in seinem Gepäck finden, auch wenn dieser sich nicht darin befindet. Hierzu mutzt ihr einen Schicksalspunkt und wirft einen W10:

- 1-4: Der gewünschte Gegenstand konnte nicht gefunden werden.
- 5-9: Ein ähnlich nützlicher Gegenstand konnte gefunden werden.
- 10: Der Charakter konnte genau den gewünschten Gegenstand im Gepäck finden.

### Bauernfänger

Der Charakter kann eine Person in Sicht dazu bringen, einen seiner Wiederholungswürfe auf einen gerade geworfenen Wurf anzuwenden.

### Gefährte

Durch diesen Stunt erhältst du einen Verbündeten, also jemanden, der dir innerhalb und außerhalb von Konflikten hilfreich zur Seite stehen kann. Gefährten bekommen ihre eigenen EP und können mit diesen gesteigert werden. Zu Beginn ist es ein Stufe 25 Begleiter, siehe dazu auch Begleiter und beschworene Kreaturen.

### Tiergefährte

Dein Charakter hat einen Gefährten aus dem Tierreich. Dein Tiergefährte kann aus jeder Gegend kommen. Tiergefährten bekommen ihre eigenen EP und können mit diesen gesteigert werden. Zu Beginn ist es ein Stufe 25 Begleiter, siehe dazu auch Begleiter und beschworene Kreaturen.

Ein Waschbär könnte Fingerfertigkeit besitzen, da er mit seinen geschickten Händchen Dinge ganz gut manipulieren kann; ein Löwe könnte Einschüchtern besitzen.

Wenn dein Tiergefährte groß genug ist, kann er dir als Reittier dienen und gibt dir eine Vorteil auf Reiten-Proben.

```admonish tip
Ein Waschbär könnte List besitzen, da er mit seinen geschickten Händchen Dinge ganz gut manipulieren kann; ein Löwe könnte Einschüchtern besitzen.
```

Wenn dein Tiergefährte groß genug ist, kann er dir als Reittier dienen und gibt dir eine Vorteil auf Bewegungs-Proben.

## Gefährten

### Besondere Bewegung

Wenn du dich auf besondere Art und Weise bewegst (besonderes Fahrzeug, Teletortation, Fliegen), dann hat auch dein Gefährte die Möglichkeit, sich auf diese Art und Weise fortzubewegen. Wenn du dich ganz normal bewegst, braucht dein Gefährte diese Erweiterung nicht.

### Kompetent

Du kannst deinem Gefährten mit dieser Erweiterung 2 zusätzliche Fertigkeitspunkte geben.

### Kontakt

Der Gefährte kann dich immer erreichen (und du ihn), selbst unter außergewöhnlichen Umständen. Vielleicht habt ihr eine übersinnliche Verbindung, oder er hat einen Sender in seinem Körper implantiert. Bei Tiergefährten sind es nur Sinneseindrücke.

### Rufen

Du kannst deinen Gefährten zu dir rufen, egal, wo du gerade bist. Normalerweise braucht er ein paar Minute, bis er bei dir ist, aber du kannst einen Schicksal-Punkt ausgeben, damit er dich innerhalb einer Runde erreicht. Wenn du Ausgeschaltet wirst, verschwindet oder flieht dein Gefährte.

### Unabhängig

*Nur für Gefährten* <!-- markdownlint-disable-line no-emphasis-as-heading -->

Der Gefährte kann unabhängig von dir handeln, ohne dass du einen Schicksal-Punkt ausgeben musst. Außerdem kann er auf eure Schicksals-Punkte zugreifen und bis zu zwei deiner Aspekte einsetzen, wenn sie auch für ihn relevant sind.
