# Waffen

Hier findet ihr Vorschläge für Ausrüstung inklusive Werte und Preise. Einige Preise wurden bewusst weggelassen, damit diese Gegenstände als Belohnungen genutzt werden können. Die Liste ist nicht vollständig und soll euch lediglich als Inspiration dienen.

Die Ausrüstung ist in verschiedene Epochen unterteilt, wobei jede Epoche ihr eigenes Niveau besitzt. Dadurch müssen die Werte für Waffen und Rüstungen nicht kontinuierlich ansteigen. Das Mischen von Epochen stellt jedoch eine größere Herausforderung dar.

Das Attribut gibt den Mindestwert an, der benötigt wird, um einen Gegenstand zu führen oder zu tragen. Rüstungen können nicht übereinander getragen werden, um den Rüstungswert (RW) zu erhöhen.

## Mittelalter (ca. 5. bis 15. Jahrhundert)

| **Waffen**           | **Kategorie**              | **Attribut** | **Schaden** | **Kosten** |
| -------------------- | -------------------------- | ------------ | ----------- | ---------- |
| Kurzbogen            | Bogen                      |              | 0           | 1          |
| Langbogen            | Bogen                      |              | 1           | 2          |
| Kompositbogen        | Bogen                      | ST 2         | 2           | 3          |
| Kriegsbogen          | Bogen                      | ST 3         | 3           | 3          |
| Handarmbrust         | Armbrust                   |              | 0           | 1          |
| Armbrust             | Armbrust                   | ST 2         | 2           | 2          |
| Kriegsarmbrust       | Armbrust                   | ST 3         | 3           | 3          |
| Langdolch            | Kurze Klinge               |              | 1           | 2          |
| Kurzschwert          | Kurze Klinge               | ST 2         | 2           | 2          |
| Breitschwert         | Lange Klinge               | ST 2         | 1           | 2          |
| Langschwert          | Lange Klinge               | ST 2         | 2           | 2          |
| Bastard-Schwert      | Lange Klinge               | ST 4;3¹      | 3;4¹        | 3          |
| Zweihänder           | Lange Klinge/Zweihandwaffe | ST 3         | 4           | 3          |
| Wurfbeil             | Axt/Wurfwaffe              |              | 0           | 1          |
| Einhand-Axt          | Axt                        |              | 1           | 2          |
| Streitaxt            | Axt                        | ST 2         | 2           | 2          |
| Kriegsaxt            | Axt                        | ST 4;3¹      | 3;4¹        | 3          |
| Zweihand-Axt         | Axt/Zweihandwaffe          | ST 3         | 4           | 3          |
| Knüppel/Improvisiert | Wuchtwaffe                 |              | 0           | 0          |
| Peitsche⁴            | Wuchtwaffe                 |              | 0           | 1          |
| Peitsche mit Klingen | Wuchtwaffe                 |              | 1           | 2          |
| Flegel               | Wuchtwaffe                 |              | 1           | 2          |
| Streitkolben         | Wuchtwaffe                 | ST 2         | 2           | 2          |
| Kriegshammer         | Wuchtwaffe                 | ST 4;3¹      | 3;4¹        | 3          |
| Dolch                | Stichwaffe/Wurfwaffe       |              | 1           | 1          |
| Stilett              | Stichwaffe                 | GE 2         | 2           | 2          |
| Stab                 | Stangenwaffe               | ST 2;1¹      | 0;1¹        | 0          |
| Speer                | Stangenwaffe/Wurfwaffe     | ST 2;1¹      | 1;2¹        | 1          |
| Luzerner Hammer      | Stangenwaffe/Zweihandwaffe | ST 4         | 4           | 4          |
| Schleuder            | Wurfwaffe                  |              | 0           | 0          |
| Netz²                | Wurfwaffe                  |              | 0           | 1          |
| Bumerang             | Wurfwaffe                  |              | 0           | 1          |
| Fäuste               | Waffenlos                  |              | 0           | 0          |
| Nietenhandschuh      | Waffenlos                  |              | 1           | 1          |
| Panzerhandschuh³     | Waffenlos                  | ST 2         | 2           | 3          |
| Schlagring           | Waffenlos                  |              | 1           | 2          |

1. 1 Händig; 2 Händig
2. Bei Treffer erhält der Gegner einen Nachteil auf Kampf- und Bewegungsmanöver. Zur Befreiung ist eine Reaktion-Probe nötig.
3. Damit ist Parieren möglich
4. Bei einem Treffer wird der Gegner mit der Peitsche umschlungen. Bei Erfolg ist der Gegner umschlungen und erleidet einen Nachteil auf Kampf- und Bewegungsmanöver. Zur Befreiung ist eine Reaktion-Probe nötig, welches jedoch um "Erfolge" erschwert wird.

## Renaissance und Wilde Westen (ca. 14. bis 18. Jahrhundert)

| **Waffen**         | **Kategorie**              | **Attribut** | **Schaden** | **Kosten** |
| ------------------ | -------------------------- | ------------ | ----------- | ---------- |
| Degen              | Stichwaffe                 | GE 2         | 2           | 2          |
| Hellebarde 2H      | Stangenwaffe/Zweihandwaffe | ST 2         | 2           | 2          |
| Steinschloßpistole | Pistole                    | GE 2         | 0           | 3          |
| Peacemaker         | Pistole                    | GE 2         | 1           | 3          |
| Muskete            | Gewehr                     | GE 2         | 1           | 3          |
| Springfield M1873  | Gewehr                     | GE 2         | 1           | 4          |
| Winchester M1873   | Gewehr                     | GE 2         | 2           | 4          |
| Sharps Rifle       | Gewehr                     | GE 3         | 3           | 5          |

## Industrialisierung und 19. Jahrhundert (ca. 1760 bis 1900)

| **Waffen**        | **Kategorie** | **Attribut** | **Schaden** | **Kosten** |
| ----------------- | ------------- | ------------ | ----------- | ---------- |
| Bowie-Messer      | Nahkampf      |              | 0           | 1          |
| Bajonett          | Nahkampf      |              | 0           | 1          |
| Colt M1911        | Pistole       |              | 1           | 2          |
| Luger P08         | Pistole       |              | 1           | 2          |
| Mauser Model 1871 | Gewehr        |              | 2           | 3          |
| Gewehr 98         | Gewehr        |              | 2           | 3          |

## Moderne (ca. 1900 bis heute)

| **Waffen**  | **Kategorie** | **Attribut** | **Schaden** | **Kosten** |
| ----------- | ------------- | ------------ | ----------- | ---------- |
| KM 2000     | Nahkampf      |              | 0           | 1          |
| Glock 17    | Pistole       |              | 1           | 2          |
| M9          | Pistole       |              | 1           | 2          |
| UZI         | MP            |              | 1           | 3          |
| MP5         | MP            |              | 1           | 3          |
| AK-47       | Gewehr        |              | 2           | 3          |
| M16-Gewehr  | Gewehr        |              | 2           | 3          |
| G36         | Gewehr        |              | 3           | 4          |
| Handgranate | Wurfwaffe     |              | 3           | 1          |

## Science Fiction (ferner? Zukunft)

| **Waffen**      | **Kategorie** | **Attribut** | **Schaden** | **Kosten** |
| --------------- | ------------- | ------------ | ----------- | ---------- |
| Molekularmesser | Nahkampf      |              | 0 (RD 1)    | 1          |
| Energieklinge   | Nahkampf      |              | 0 (RD 2)    | 2          |
| Laserpistole    | Pistole       |              | 1           | 2          |
| Plasmapistole   | Pistole       |              | 1 (RD 1)    | 3          |
| Laser-Gewehr    | Gewehr        |              | 2           | 3          |
| Plasma-Gewehr   | Gewehr        |              | 2 (RD 1)    | 4          |
| Railgun         | Gewehr        |              | 3           | 4          |
