# Verbesserungen

Waffen können durch bestimmte Materialien verändert werden, um gegen bestimmte Feinde besser gewappnet zu sein. \
Bei Fernkampfwaffen muss die Munition in den Materialien beschafft werden. Die Kosten für 100 Stück betragen 1.

| **Material**   | **Modifikation**                                           | **Kosten** |
| -------------- | ---------------------------------------------------------- | ---------- |
| Holz           | Voraussetzung -1, max 1 Schaden pro Treffer                | /2         |
| Stahl          | RD 1 gegen Humanoide                                       | \*2        |
| Silber         | RD 1 gegen Monster                                         | \*3        |
| Adamantit      | RD 2 gegen Humanoide                                       | \*5        |
| Mithril        | RD 2 gegen Monster                                         | \*6        |
| Kristall       | RD 1 gegen Geister, zerspringt bei Humanoiden und Monstern | -          |
| Obsidian       | RD 2 gegen Geister, zerspringt bei Humanoiden und Monstern | -          |
| Opal           | RD 3 gegen Geister, zerspringt bei Humanoiden und Monstern | -          |
| Drachenknochen | RD 3 gegen Monster                                         | -          |
| Adamantium     | RD 3 gegen Humanoide                                       | \*6        |
| Meteoritenerz  | RD 2 gegen Humanoide und Monster                           | -          |
