# Alchemie

## Voraussetzung

Aspekt: Alchemist
Fertigkeit: Fachkenntnisse

Optional zum Sammeln:
Aspekt: Kräuterkundiger
Fertigkeit: Fachkenntnisse

## Grundausstattung

- Kessel – Alchemiekessel
- Feuerstelle (z.B. Lagerfeuer)
- Kochlöffel – Holzlöffel
- Behälter für:
  - Tränke: Glasphiolen
  - Elixiere: Ampullen
  - Medizin: Holztiegel

## Allgemeines

Um eine Tinktur (darunter fällt alles nachfolgende) zu brauen, benötigt man einige Zeit, ein geeignetes Behältnis, diesen erforderlichen Utensilien und die Zutaten.

### Zutaten

#### Suchen

Zum Finden der Zutaten benötigt ihr eine Fachkenntnisse-Probe und ihr benötigt Erfolge in Höhe des Rangs der Tinktur, sowie Rang der Tinktur in Stunden Zeit.

#### Kaufen

Die Kosten der Zutaten betragen den halben Rang der Tinktur in Ressourcen

### Brauen

Das Brauen ist eine Fachkenntnisse-Probe und ihr benötigt Erfolge in Höhe des Rangs der Tinktur. Außerdem benötigt es Rang in halbe Stunden.

```admonish example title='Beispiel'
Für eine Zutanten einer Rang 4 Tinktur werden entweder 2 Ressourcenpunkte benötigt oder eine Fachkenntnisse-Probe mit 4 Erfolgen und 4 Stunden Zeit.<br/>
Um diese Tinktur herzustellen, wird ebenfalls Fachkenntnisse-Probe mit 4 Erfolgen und 2 Stunden Zeit benötigt.
```

Bei einem Misserfolg gehen Pflanzen und Behälter verloren.

## Tränke

Der Rang beträgt die Regeneration der Wunden.

Der Preis eines Trankes entspricht Rang x 2 Ressourcen
