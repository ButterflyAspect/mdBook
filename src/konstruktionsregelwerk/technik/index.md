# Technik

## Voraussetzung

Aspekt: z.B. Schmied
Fertigkeit: Fachkenntnisse

Optional zum Sammeln:
Aspekt: z.B. Bergmann
Fertigkeit: Fachkenntnisse

## Grundausstattung

- Werkstatt z.B. Schmiede
- Das Material, das verarbeitet werden soll, z.B. Eisenbarren

## Allgemeines

Um einen Gegenstand (darunter fällt alles nachfolgende) herzustellen, benötigt man einige Zeit, eine Werkstatt, die erforderlichen Utensilien und das Material.

### Material

#### Suchen

Zum Finden des Materials benötigt ihr eine Fachkenntnisse-Probe und ihr benötigt Erfolge in Höhe des Rangs des Gegenstandes, sowie Rang des Gegenstandes in Stunden Zeit.

#### Kaufen

Die Kosten des Materials betragen den halben Rang des Gegenstandes in Ressourcen

### Herstellen

Das Herstellen ist eine Fachkenntnisse-Probe und ihr benötigt Erfolge in Höhe des Rangs des Gegenstandes. Außerdem benötigt es Rang in Stunden Zeit.

```admonish example title='Beispiel'
Für die Materialien eines Rangs 2 Gegenstandes werden entweder 1 Ressourcenpunkt benötigt oder eine Fachkenntnisse-Probe mit 2 Erfolgen und 2 Stunden Zeit.<br/>
Um diesen Gegenstand herzustellen, wird ebenfalls Fachkenntnisse-Probe mit 2 Erfolgen und 2 Stunden Zeit benötigt.
```

Bei einem Misserfolg gehen die Materialien verloren.

## Schmieden

Der Rang beträgt den Preis des Gegenstandes.
