# Zauber Wirken

Ein Zauber kann nur gewirkt werden, wenn der Charakter noch mindestens die entsprechende Menge Mana zur Verfügung hat.

Um einen Zauber zu wirken, wird eine Mysterien-Probe abgelegt. Ist der Wurf mindestens ein Erfolg, ist der Zauber erfolgreich.

```admonish attention title="Achtung"
Das angegebene Mana wird dem Charakter abgezogen, unabhängig davon, ob der Zauber erfolgreich war oder nicht.
```

[Treffer](/grundregelwerk/konflikte/kampf.html#treffen) und [Schaden](/grundregelwerk/konflikte/kampf.html#schaden) wird wie beim Waffenkampf abgehandelt. Der Waffenschaden wird druch Stärke des Zaubers ersetzt und der Rüstungswert(RW) hat keinen effekt auf Zauber.

```admonish tip
Bestimmte Materialien schützen euch auch vor magischen Angriffen.
```

## Misserfolg

Wenn der Zauber misslingt, wird auf die [Zusatzeffekte](/magieregelwerk/zusatzeffekte/index.md) Tabelle gewürfelt.

## Kritischer Misserfolg

Wenn du einen Patzer würfelst, werden dich die schlimmen Dinge heimsuchen, die in der [Patzertabelle](/magieregelwerk/zusatzeffekte/patzertabelle.md) beschrieben sind.

## Schaden

Der Schaden wird wie beim [Waffenkampf](/grundregelwerk/aktionen/kampf.html#kampfschaden) behandelt, jedoch ohne den Rüstungswert, es sei denn, es handelt sich um magische Rüstung.

```admonish example title='Beispiel'
Bei einem Feuerball mit drei Erfolgen wird 4 Schaden verursacht und dauert 1 Kampfrunden.
```

## Heilung

Im Gegensatz zur normalen [Heilung](/grundregelwerk/konflikte/verletzungen.m#heilung) kann magische Heilung auch mittlere Folgen heilen, allerdings nur Wunden. Die Heilung entspricht den Treffern plus der Stärke des Zaubers. Auch magische Heilung benötigt Zeit, und zwar in Höhe der Erfolge in Minuten.

## Materielle Komponenten

Magier benutzen oft Bücher und materielle Komponenten, um ihre Zauber zu unterstützen. Diese werden im Spiel als Sunts oder Schübe dargestellt. Ein Buch kann ein permanenter Aspekt deines Charakters sein oder ein Situationsaspekt, den du zuvor erschaffen hast. Materielle Komponenten sind normalerweise Schübe, da sie sich bei Gebrauch verbrauchen.

## Fokus

Ein Fokus ist die einzige Möglichkeit, Magie zu speichern, man muss ihn nur berühren und die Kraft fließt einfach in ihn hinein. Es kann immer nur ein Fokus vorhanden sein und es kann nur Mana in Höhe des Mysterienwertes gespeichert werden. Dieser Vorgang dauert pro Mana eine Stunde. \
Ein Fokus kann beliebig aussehen, besteht aber aus einem speziellen Material, das Magie speichern kann. Da dieses Material selten ist, kostet ein Fokus 5 Rohstoffpunkte.

### Benutzen

Das Mana aus dem Fokus zu benutzen ist genauso einfach, wie es in den Fokus zu übertragen. Dazu muss er erneut berührt werden und kann dann für eine Aktion so viel Mana aus dem Fokus entnehmen, bis sein persönliches Maximum erreicht ist.
