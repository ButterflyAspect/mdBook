# Zaubern

## Werte für Zauber

Ein Zauber hat verschiedene Attribute, die im Spiel relevant sind.

- **Mana (M)**:

    Die Menge an magischer Essenz, die benötigt wird, um den Zauber zu wirken. Um einen Zauber mit einem Mana von 2 zu wirken, muss der Charakter mindestens 2 Mana zur Verfügung haben und diese bei der Ausführung des Zaubers verbrauchen.

- **Aktionen (A)**:

    Gibt an, wie viele Aktionen notwendig sind, um den Zauber zu wirken.

- **Stärke (S)**:

    Bestimmt die Wirksamkeit des Zaubers und kann aber durch bestimmte Faktoren erhöht werden. Die Stärke ist nicht nur ein Indikator für den Schaden, sondern kann auch für die Anzahl der beschworenen Kreaturen stehen oder die Dauer eines Zaubers sowie die Höhe des Bonus für einen Buff.

- **Reichweite (R)**:

    Die maximale Entfernung in Metern, die der Zaubernde vom Ziel haben darf, damit der Zauber noch wirkt. Die Reichweite hängt von der Zauberformel ab. Es ist wichtig zu beachten, dass die Reichweite nicht den Wirkungsbereich des Zaubers beschreibt.

- **Wirkungsbereich (W)**:

    Beschreibt die Größe des Bereiches in Metern, auf den der Zauber wirkt. Der Bereich kann ein Kreis oder ein Kegel sein. Der Kreis wird vom Eck eines Feldes aus gemessen und der Kegel vom Zaubernden.

### Zauber erstellen

Wenn du einen Zauber erstellst, entscheide, was er bewirken soll. Willst du einen Fluss umleiten? Jemanden in die Flucht schlagen? Einen Dschinn beschwören? Ein neuer Zauber kostet 5 Zauberpunkte. Je nach gewünschter Wirkung erstellst du einen Zauberspruch mit entsprechender Beschreibung und folgenden Werten:

- **Mana**: 1
- **Aktionen**: 1
- **Stärke**: 1
- **Reichweite**: 5
- **Wirkungsbereich**: -

Da die Stärke für alle Zauber genutzt wird, liegt es im ermessen des Spielleiters welche einheit sie bekommt. Für die Zeiteintelung empfiehlt sich: Runden, Minuten, Stunden, Tage

Um die verschiedenen Werte eines Zaubers in einklang zu bringen, verwenden wir die folgende Gleichung:

\\[ M + A = S + \frac{R}{5} + {W} \\]

**Anwendung der Formel:**

- Addiere die Manakosten und die benötigten Aktionen auf der linken Seite der Gleichung.
- Berechne die Reichweite geteilt durch 5 und addiere die Stärke auf der rechten Seite der Gleichung.
- Bei ungeraden Zahlen wird immer aufgerundet.

**Gleichgewicht herstellen:**

- Die Summe der Manakosten und der Aktionen sollte gleich der Summe aus Reichweite/5 und Stärke sein.
- Falls die Werte nicht übereinstimmen, kannst du Anpassungen vornehmen:
  - Erhöhe oder verringere die Manakosten, die Aktionen, die Reichweite oder die Stärke, bis beide Seiten der Gleichung gleich sind.

### Zauber Verbessern

Mit den folgenden Verbesserungen kann man seine Zauber anpassen, sowohl bei der Erstellung als auch später. Die Verbesserungen sind auch mehrmals möglich:

- Für 2 Zauberpunkt wird die Stärke des Zaubers um eins erhöht.
- Für 5 Zauberpunkte wird das benötigte Mana um 1 reduziert, wobei das Minimum 1 beträgt.
- Für 5 Zauberpunkte wird dem Zauber ein zusätzliches Ziel hinzugefügt, sodass alle Ziele vom Zauber betroffen sind.
- Für 1 Zauberpunkt wird die Reichweite des Zaubers um 5 Meter erhöht.
- Für 3 Zauberpunkte wird die Anzahl der benötigten Aktionen für den Zauber um eins verringert, wobei das Minimum 1 beträgt.
- Für 3 Zauberpunkte wird die Form des Zaubers geändert, z.B. von einem Punkt zu einer Kreisfläche oder einem Kegel. Die Erfolge werden auf die Ziele aufgeteilt.
- Für 5 Zauberpunkte für besondere Effekte z.B. Blenden, Betäuben etc.

### Zauber vergessen

Am Ende eines Szenarios ist es möglich, Zauber zu vergessen und neue zu lernen. Alle Zauberpunkte werden zurückerstattet.
