# Zusatzeffekte

1. Kein weiterer Effekt
2. Das Wirken des Zaubers löst ein Gewitter aus Amphibien aus, die in einem Umkreis von 10 Meter um den Zauberkundigen herum zu Boden regnen. Allen, die sich in diesem Bereich aufhalten, müssen eine [Reaktion](/grundregelwerk/konflikte/rettungswuerfe.md#reaktion)s-Probe schaffen, sonst rutschen sie auf einem Frosch aus und stürzen zu Boden oder werden von besonders großen Lurchen getroffen und erleiden 1d3 Punkte Schaden.
3. Ein nicht spürbarer, lautloser Wind lässt Haare und Kleidung des Magiers für \[[1d10]] Minuten lang heftig flattern.
4. Es regnet aus heiterem Himmel Asche auf den Zauberwirkenden herab, in einem Umkreis von 10 Meter erhalten alle einen Nachteil auf Sicht.
5. Der Tastsinn des Magiers wird für \[[1d10]] Stunden verbessert, er kann Vibrationen innerhalb eines 10 Meter Radius erspüren.
6. Dem Magier wächst für \[[1d10]] Stunden ein Bart aus Federn.
7. Der Magier kann für die nächste Minute nicht sprechen. Falls er es doch versucht, schweben rosa Bläschen aus seinem Mund.
8. Nach dem Wirken des Zaubers, schreit der Magier für \[[1d10]] Minuten, wenn er sprechen will.
9. Es erscheint für \[[1d10]] Minuten ein Spiegelbild von dem Magier und imitiert ihn.
10. Der Magier ist \[[1d10]] Minuten lang unsichtbar.
11. Nach dem Zauber spricht der Zaubernde \[[1d10]] Minuten lang eine ihm fremde Sprache (kann immer noch zaubern).
12. Dem Magier fallen bis Mitternacht alle Haare aus.
13. Die Augen des Magiers ändern permanent ihre Farbe. Ein Zauber wie "Fluch entfernen" kann diesen Effekt beenden.
14. Eine Stunde lang fangen plötzlich alle Pflanzen, die größer als einen halben Meter sind und nicht zu den Gräsern gehören, an zu kichern und zittern dabei leicht, wenn sich ihnen der Magier auf weniger als drei Meter nähert.
15. Ein Rattenschwarm erscheint neben dem Zauberwirkenden.
16. In den nächsten \[[1d10]] Minuten wachsen, in einem Radius von 10 Metern, alle Pflanzen um den Magier.
17. In den nächsten \[[1d10]] Minuten verdorren, in einem Radius von 10 Metern, alle Pflanzen um den Magier.
18. Der Magier schwebt plötzlich in einem Meter Höhe über dem Boden. Wenn er es versucht, stellt er fest, dass er sich durch reine Willenskraft sogar langsam (halbe Schrittgeschwindigkeit) fortbewegen kann. Der Effekt bricht nach \[[1d10*10]] Minuten abrupt ab.
19. Der Magier zischt für den restlichen Tag bei jedem Wort das "s" wie eine Schlange.
20. Die Nase des Magiers leuchtet für \[[1d10]] Stunden rot.
21. Ein magischer Gegenstand im Umkreis von 10 Metern verschwindet für \[[1d10]] Stunden.
22. Pilze sprießen in einem Radius von 5 Metern um den Magier und verschwinden nach \[[1d10]] Minuten. Wenn einer der Pilze gegessen wird, muss eine [Zähigkeit](/grundregelwerk/aktionen/rettungswuerfe.md#z%C3%A4higkeit)s-Probe geschafft werden. Bei einem Fehlschlag erleidet der Konsument \[[1d4]] Schaden, bei Erfolg erhält er \[[1d4]] Leben.
23. Die Füße des Magiers sinken in den Boden und machen ihn für \[[1d10]] Minuten unbeweglich. Dies hat keine Auswirkung, wenn der Magier beim Wirken des Zaubers nicht auf dem Boden steht.
24. Es erscheint über dem Kopf des Magiers Wasser, das alle in einem 10 Meter Radius durchnässt.
25. Der Zauberer ist 1d10 Minuten lang von einem schwachen, aber abstoßenden Geruch umgeben. Er erhält einen Nachteil auf alle [Diplomatie](/grundregelwerk/attribute_fertigkeiten/empathie.md#diplomatie)-Proben.
26. Eine zufällige Person im 10 Meter Umkreis wächst \[[1d10*5]] cm in die Höhe. Bis zum Ende des Tages hat er wieder seine normale Größe.
27. In den nächsten \[[1d10]] Stunden kann der Magier nicht lesen, da alle Buchstaben durcheinander erschienen.
28. Der Magier kann \[[1d10]] Stunden lang nur mit Tieren sprechen.
29. Die Erscheinung des Magiers wirkt dreckig und bringt einen Nachteil auf [Diplomatie](/grundregelwerk/attribute_fertigkeiten/empathie.md#diplomatie)-Proben, bis er sich reinigt.
30. Eine zufällige Person im 10 Meter Umkreis schrumpft \[[1d10*5]] cm in der Höhe. Bis zum Ende des Tages hat er wieder seine normale Größe.
31. Für die nächsten \[[1d10]] Stunden wechselt eine zufällige Person in 10 Metern Umkreis das Geschlecht.
32. Die Nase des Magiers läuft für die nächsten \[[1d10]] Stunden ununterbrochen.
33. Ein lebloses Objekt innerhalb von 10 Metern wird für \[[1d10]] Tage zum Leben erweckt.
34. Bis zum Ende des Tages wechselt der Hautton des Magiers alle 30 Minuten seine Farbe innerhalb des Regenbogenspektrums.
35. Dem Magier wächst für \[[1d10]] Stunden ein drittes Auge auf der Stirn, er bekommt einen Vorteil auf [Wahrnehmung](/grundregelwerk/attribute_fertigkeiten/ituition.md#wahrnehmung)s-Proben.
36. Der Magier wird \[[1d10]] Meter vorwärts teleportiert. Er kann dadurch nicht in den Tod stürzen oder durch Mauern/undurchdringliche Objekte. Scheiben oder Gitter sind möglich.
37. Eine zufällige Person in 10 Metern wird für \[[1d10]] Stunden beschwipst.
38. Eine für den Ort passende Kreatur erscheint und es wird \[[1d10]] gewürfelt, um festzustellen, ob das Tier freundlich (gerade) oder aggressiv (ungerade) gegenüber dem Magier ist.
39. Dem Magier wachsen für \[[1d10]] Stunden Kiemen und er kann damit zusätzlich unter Wasser atmen.
40. Bis zum Ende des Tages erscheint neben dem Magier ein Phantomross.
41. Ein Regenbogen erscheint über dem Magier und begleitet ihn für die nächsten \[[1d10]] Stunden, wobei er auf magische Weise seinem Schritt folgt.
42. Der Körper des Magiers wird für \[[1d10]] Stunden durchsichtig, aber seine Kleidung bleibt sichtbar.
43. Eine unsichtbare Barriere entsteht um den Magier herum, die für \[[1d10]] Minuten undurchdringlich ist.
44. Der Magier wird von einem sanften, beruhigenden Glühen umhüllt, das \[[1d10*10]] Minuten anhält und allen, die ihn sehen, ein Gefühl von Frieden vermittelt.
45. Ein Schwarm leuchtender Schmetterlinge erscheint und begleitet den Magier für die nächsten \[[1d10]] Stunden.
46. Alle Uhren im Umkreis von 10 Metern bleiben für \[[1d10]] Minuten stehen.
47. Der Magier kann für die nächsten \[[1d10]] Minuten durch feste Gegenstände sehen, als ob sie aus Glas wären.
48. Ein Regenschauer aus kleinen, bunten Blütenblättern fällt über den Magier und seine Umgebung für \[[1d10]] Minuten.
49. Der Magier kann für \[[1d10]] Stunden die Gefühle anderer Menschen spüren.
50. Eine sanfte Melodie ertönt um den Magier herum und begleitet ihn für die nächsten \[[1d10]] Minuten.
51. Der Magier kann für \[[1d10]] Minuten fliegen, aber nur rückwärts.
52. Ein lebendiger Schatten des Magiers tanzt unabhängig von ihm für die nächsten \[[1d10]] Minuten.
53. Ein schwacher Glanz umhüllt die Hände des Magiers, und er kann für \[[1d10]] Minuten im Dunkeln sehen.
54. Der Magier kann für \[[1d10]] Minuten durch Wände hindurch hören.
55. Ein unsichtbarer Diener erscheint und erfüllt den Magier für die nächsten \[[1d10]] Minuten kleine Dienste.
56. Der Magier wird für \[[1d10]] Minuten von einer Aura des Schweigens umgeben, in der jeglicher Lärm erstickt wird.
57. Ein Phantomstift erscheint, der für \[[1d10]] Minuten schreibt, was der Magier denkt, ohne dass er ihn berührt.
58. Der Magier kann für \[[1d10]] Minuten mit Objekten verschmelzen und durch sie hindurch gehen.
59. Ein winziger, tanzender Geist erscheint und führt den Magier für die nächsten \[[1d10]] Minuten in einem fröhlichen Tanz an.
60. Ein Regenbogenstrahl schießt aus den Augen des Magiers und enthüllt für \[[1d10]] Minuten unsichtbare Schätze oder magische Symbole in der Umgebung.
61. Der Magier kann für \[[1d10]] Minuten jede Sprache verstehen, dafür nicht mehr sprechen.
62. Ein leises Flüstern umgibt den Magier für \[[1d10]] Minuten und gibt ihm einen Nachteil auf alle [Wahrnehmung](/grundregelwerk/attribute_fertigkeiten/ituition.md#wahrnehmung)s-Proben.
63. Der Magier kann für \[[1d10]] Minuten durch Zeit hindurch sehen und vergangene Ereignisse in einem bestimmten Ort miterleben.
64. Ein unsichtbarer Gefährte erscheint und gibt dem Magier für \[[1d10]] Minuten Ratschläge.
65. Die Kleidung des Magiers wird für \[[1d10]] Minuten lebendig und formt sich nach seinen Wünschen.
66. Ein Wirbelwind aus Blättern und Blumen umgibt den Magier und bietet ihm für \[[1d10]] Minuten Schutz vor Geschossen.
67. Die Schatten des Magiers dehnen sich für \[[1d10]] Minuten in eine malerische Szenerie aus, die seine Geschichten visuell begleitet.
68. Ein lebendiges Abbild des Magiers erscheint für \[[1d10]] Minuten und wiederholt alles, was er sagt, mit einem Echo.
69. Der Magier hinterlässt für \[[1d10]] Minuten Schritte auf allen Oberflächen.
70. Ein Glanz umhüllt den Magier, der für \[[1d10]] Minuten eine angenehme Wärme verbreitet.
71. Der Magier wird für \[[1d10]] Minuten von einem schillernden Licht umgeben.
72. Ein Lächeln erscheint auf dem Gesicht des Magiers und verbreitet gute Laune um ihn herum für \[[1d10]] Minuten.
73. Ein ständiger, angenehmer Duft begleitet den Magier für \[[1d10]] Stunden, der seine Anziehungskraft erhöht.
74. Ein magisches Hologramm projiziert Bilder der Gedanken des Magiers für \[[1d10]] Minuten in die Luft.
75. Die Haut des Magiers schimmert für \[[1d10]] Stunden in den Farben des Regenbogens.
76. Der Magier kann für \[[1d10]] Minuten durch Wände sehen, aber nur durch solche, die aus Holz bestehen.
77. Ein unsichtbarer Käfer schwirrt um den Magier herum und flüstert ihm für \[[1d10]] Minuten kleine Geheimnisse zu.
78. Der Magier kann für \[[1d10]] Minuten Geräusche imitieren, als kämen sie aus einer anderen Richtung.
79. Ein Farbnebel umhüllt den Magier und ändert seine Kleidungsfarben für \[[1d10]] Stunden.
80. Der Magier kann für \[[1d10]] Minuten mit der Natur kommunizieren und einfache Anfragen an Pflanzen und Tiere stellen.
81. Ein Miniatur-Regenschauer umgibt den Magier und erzeugt für \[[1d10]] Minuten einen leichten Nieselregen um ihn herum.
82. Der Magier kann für \[[1d10]] Minuten die Emotionen einer Person fühlen, indem er sie berührt.
83. Der Magier kann für \[[1d10]] Minuten mit einer Stimme sprechen, die von einer mysteriösen Kraft getragen wird, die nur er hören kann.
84. Ein magischer Schatten des Magiers tanzt für \[[1d10]] Minuten unabhängig von ihm und spielt dabei mit Licht und Dunkelheit.
85. Der Magier kann für \[[1d10]] Minuten mit Schatten interagieren, sie formen und manipulieren.
86. Der Magier kann für \[[1d10]] Minuten flüstern und sicher sein, dass seine Worte nur von denjenigen gehört werden, die er adressiert.
87. Ein lebendiger Regenschirm erscheint über dem Magier und schützt ihn für \[[1d10]] Minuten vor Regen.
88. Der Magier kann für \[[1d10]] Minuten schattenhafte Illusionen erschaffen, die sich auf den Boden oder Wände projizieren.
89. Ein schimmernder Nebel umhüllt den Magier und gibt ihm für \[[1d10]] Minuten einen Vorteil auf [List](/grundregelwerk/attribute_fertigkeiten/geschick.md#list)-Proben und einen Nachteil auf alle [Wahrnehmung](/grundregelwerk/attribute_fertigkeiten/ituition.md#wahrnehmung)s-Proben.
90. Der Magier kann für \[[1d10]] Minuten mit seinem Schatten interagieren, um einfache Botschaften zu übermitteln.
91. Der Magier kann für \[[1d10]] Minuten durch Berührung das Alter eines Gegenstands oder einer Person bestimmen.
92. Ein leuchtendes Symbol erscheint auf der Stirn des Magiers und gibt ihm für \[[1d10]] Minuten einen Vorteil auf Diplomatie.
93. Der Magier kann für \[[1d10]] Minuten mit leblosen Objekten kommunizieren und einfache Antworten erhalten.
94. Ein schimmernder Schleier umhüllt den Magier und verleiht ihm für \[[1d10]] Minuten eine gespenstische Aura.
95. Der Magier kann für \[[1d10]] Minuten durch die Augen eines Tieres in seiner Nähe sehen.
96. Der Magier kann für \[[1d10]] Minuten das Gewicht von Objekten in seiner Nähe beeinflussen.
97. Ein leises Kichern folgt dem Magier für \[[1d10]] Minuten und verbreitet eine fröhliche Stimmung.
98. Der Magier kann für \[[1d10]] Minuten leichte Objekte telekinetisch bewegen.
99. Der Magier kann für \[[1d10]] Minuten einen unsichtbaren Pfad in die Schatten erschaffen und darauf gehen.
100. Der Magier kann für [[1d10]] Minuten den Geruch von Dingen erschnuppern, die sich nicht in seiner Nähe befinden.
