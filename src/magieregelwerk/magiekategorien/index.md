# Magiekategorien

Um Magie wirken zu können, benötigt man einen Zauberspruch. Zum Beispiel: **Ausgebildet in Elementarmagie** oder **Affinität für Elementarmagie** könnte bedeuten, dass du Elementarmagie beherrschst.

## Allgemeine Arten

- Weiße Magie (Heilung, Verteidigung und Kommunikation)
- Weissagung
- Element
- Transmutation
- Beschwörungsmagie

## Geächtete Arten

- Schwarze Magie (Flüche, Gedankenkontrolle)
- Gestaltwandel
- Illusionsmagie
- Nekromantie
- Zeitmagie
