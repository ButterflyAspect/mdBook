# Zauberbeispiele

## Weiße Magie

### Heilung

Der Zaubernde heilt den Verzauberten siehe dazu [Heilung](/grundregelwerk/konflikte/verletzungen.md#heilung).

- Zauberpunkte Kosten: 5
- Mana (M): 1
- Aktionen (A): 1
- Stärke (S): 1
- Reichweite (R): 1
- Wirkungsbereich (W): -

### Schutzaura

Der Zaubernde erschafft eine magische Schutzaura um sich herum. Die Aura strahlt von ihm ab und ist von Magiebegabten zu erkennen. Sie ist für alle Wirkungsbereichen der Magie undurchdringlich, worunter auch magische Angriffe oder Verwandlungen fallen. Die Schutzaura kann `Stärke x 2` Wunden absorbieren, bevor sie in sich zusammenfällt. Der Zauber kann maximal `Stärke Minuten` aufrechterhalten werden.

- Zauberpunkte Kosten: 5
- Mana (M): 1
- Aktionen (A): 1
- Stärke (S): 1
- Reichweite (R): 0
- Wirkungsbereich (W): 1

## Weissagung

### Blick in die Zukunft

Der Zaubernde erhält eine Vision, die ihm bis zu `Stärke x 10 Minuten` in die Zukunft blicken lässt. Diese Vision kann wertvolle Hinweise auf bevorstehende Ereignisse geben. Die Genauigkeit und Klarheit der Vision hängen von Ergebis des Zaubers ab. So kann die Vision klar und detailliert oder auch verschwommen oder missverständlich sein.

- Zauberpunkte Kosten: 5
- Mana (M): 2
- Aktionen (A): 1
- Stärke (S): 3
- Reichweite (R): 0
- Wirkungsbereich (W): -

## Elementar Magie

### Feuerball

Der Zaubernde schleudert einen Schädelgrößen lodernden Feuerball auf sein Ziel. Beim Aufprall verursacht der Zauber Schaden.

- Zauberpunkte Kosten: 5
- Mana (M): 1
- Aktionen (A): 1
- Stärke (S): 1
- Reichweite (R): 5
- Wirkungsbereich (W): -

### Dornenspeer

Der Zauberer lässt auf einer 1 Meter großen, runden Fläche Dornen aus dem Boden sprießen.

- Zauberpunkte Kosten: 5
- Mana (M): 2
- Aktionen (A): 2
- Stärke (S): 1
- Reichweite (R): 10
- Wirkungsbereich (W): 1

## Transmutation

### Metall zu Holz

Der Zaubernde verwandelt metallene Objekte in Holz. Der Zauber kann verwendet werden, um Waffen zu entwerten, Rüstungen schwächer zu machen oder metallene Hindernisse in leichter zu durchdringendes Material zu verwandeln. Die Menge des transmutierten Materials entspricht der Stärke des Zaubers `Stärke in KG` und sie dauert `Stärke Minuten` an. Der Zauber funktioniert nicht auf magisch geschützten Objekten oder Materialien.

- Zauberpunkte Kosten: 5
- Mana (M): 1
- Aktionen (A): 1
- Stärke (S): 2
- Reichweite (R): 0
- Wirkungsbereich (W): -

## Beschwörungsmagie

### Tier beschwören

Das Tier führt `Stärke` einfache Dienste/Befehle für seinen Meister aus, aber nur was im fähigkeitsbereich des Tieres liegt.

- Zauberpunkte Kosten: 5
- Mana (M): 2
- Aktionen (A): 1
- Stärke (S): 3
- Reichweite (R): 0
- Wirkungsbereich (W): -

## Schwarze Magie

### Schwarzer Fluch

Der Zaubernde spricht einen Fluch aus, der das Ziel schwächt und seine körperlichen und geistigen Fähigkeiten um `Stärke` Punkte `Stärke x 10 Minuten` reduziert. Der Fluch kann nur auf ein Ziel gleichzeitig angewendet werden und ein Ziel kann auch nur von einem Fluch gleichzeitig betroffen sein.

- Zauberpunkte Kosten: 5
- Mana (M): 5
- Aktionen (A): 3
- Stärke (S): 4
- Reichweite (R): 20
- Wirkungsbereich (W): -

## Gestaltwandel

### Fähigkeit übernehmen

Durch diese Berührung übernimmt er eine besondere Fähigkeit des Tieres, wie z.B. die Nachtsicht einer Katze, den Geruchssinn eines Hundes oder die Schwimmfähigkeiten eines Fisches. Der Zaubernde kann die Fähigkeit `Stärke x 5 Minuten` nutzen und immer nur eine Fähigkeit gleichzeitig übernehmen, und die Wirkung endet automatisch, wenn die Zeit abgelaufen ist.

- Zauberpunkte Kosten: 5
- Mana (M): 2
- Aktionen (A): 2
- Stärke (S): 4
- Reichweite (R): 0
- Wirkungsbereich (W): -

### Gestaltwandlung

Der Zaubernde verwandelt sich vollständig in ein Tier oder eine Kreatur mit den entsprechenden körperlichen Fähigkeiten und Sinnen. Die Verwandlung hält bis zu `Stärke Minuten` an oder bis der Zaubernde die Verwandlung selbst beendet.

| Stufe | Tierbeispiel              |
| ----- | ------------------------- |
| 1     | Chamäleon, Salamander     |
| 2     | Eule, Otter, Waschbär     |
| 3     | Ratte, Katze, Hund        |
| 4     | Wolf, Boa, Viper          |
| 5     | Falke, Adler, Pferd, Affe |
| 6     | Bär, Gorilla              |
| 7     | Löwe, Tiger               |

- Mana (M): 2
- Aktionen (A): 3
- Stärke (S): 5
- Reichweite (R): 0
- Wirkungsbereich (W): -

## Illusionsmagie

### Illusion erzeugen

Der Zaubernde erzeugt eine Illusion, die das Ziel täuscht. Die Illusion ist einfach und kann für maximal `Stärke Minuten` bestehen bleiben. Sie kann durch eine Wahrnehmungs-Probe gegen `5 + Stärke x 2` durchschaut werden

- Mana (M): 2
- Aktionen (A): 2
- Stärke (S): 2
- Reichweite (R): 10
- Wirkungsbereich (W): 0

## Nekromantie

### Skelett beschwören

Der Zaubernde ruft ein Skelett herbei, das ihm für `Stärke Minuten` dient. Das Skelett kann grundlegende Befehle ausführen, wie das Tragen von Gegenständen, das Öffnen von Türen oder das Blockieren eines Weges. Die `Stärke` des Zaubers bestimmt den Fertigkeitswert beim ausführen der Aufgaben. Das Skelett kann nicht sprechen und nach Abschluss seiner Aufgaben oder dem Ablauf der Zeit zerfällt das Skelett zu Staub.

- Zauberpunkte Kosten: 5
- Mana (M): 1
- Aktionen (A): 5 (Erhöht fürs balancing)
- Stärke (S): 1
- Reichweite (R): 5
- Wirkungsbereich -

## Zeitmagie

### Zeit verlangsamen

Der Zaubernde verlangsamt den Fluss der Zeit um das Ziel, wodurch es für `Stärke Runden` eine Aktion weniger zur verfügung hat.

- Mana (M): 3
- Aktionen (A): 2
- Stärke (S): 3
- Reichweite (R): 10
- Wirkungsbereich (W): -

## Kombinationsmagie

### Taschendimension

Kann auf eine andere Dimension zugreifen, in der er alles Mögliche verstauen und wieder herholen kann.

```admonish info
Da dieser Zauber zu mächtig sein kann, bekam er einige Auflagen
```

Restriktionen:
• Er muss Dinge berühren, die er reinziehen möchte
• Lebewesen können nicht reingezogen werden
• Er kann nur Dinge reinziehen, die er auch mit seiner Kraft heben/entfernen könnte
• Dinge in der anderen Dimension sind weiterhin den Gesetzen der Zeit unterlegen
• Kann nichts reinziehen, dass in Bewegung (relativ zum Charakter) ist
• Kann pro Kampfrunde max. 1-mal etwas rausholen oder reinziehen
• Kann Flüssigkeiten und Gase nur reinziehen, wenn sie in einem Behälter sind

- Zauberpunkte Kosten: 5
- Magiekategorie: Raummagie & Zeitmagie
- Mana (M): 1
- Stärke (S): 1
- Reichweite (R): 0
- Wirkungsbereich (W): -
- Aktionen (A): 12 (Erhöht fürs balancing)
