# Magische Gegenstände

Mit den Magieregeln ist es auch möglich, magische Gegenstände herzustellen.

## Herstellung

Um einen Gegenstand herzustellen, benötigt man 2 Dinge: den Gegenstand und einen Zauberspruch. Um dem Gegenstand Magie zu verleihen, sprichst du den Zauber wie gewohnt und bindest ihn in den Gegenstand ein. Bestimme, welche Handlung den Zauber aktiviert. Diese Handlung kann komplex sein oder auch nur ein gesprochenes Wort.

Wie oft der Zauber angewendet werden kann, wird durch die Anzahl der Erfolge bestimmt. Die Manakosten entsprechen den Zauberkosten multipliziert mit der Anzahl der Anwendungen, begrenzt durch das vorhandene Mana des Zauberers.

## Anwendung

Um einen magischen Gegenstand zu benutzen, muss man nur die beschriebene Handlung ausführen und der Zauber wird abgehandelt, als hätte ihn ein Zauberer gesprochen.

Um einen unbekannten magischen Gegenstand zu benutzen, muss eine Mysterien-Probe abgelegt werden, die mindestens dem Mysterienwert des Zauberers entsprechen muss.
