# Magie nutzen

Um Magie einsetzen zu können, benötigst du folgendes:

- Eine Magie-[Kompetenz](/grundregelwerk/kompetenzen/index.md) mit einer [Magiekategorie](/magieregelwerk/magiekategorien/index.md).
- Die Fertigkeit Mysterien.
- Zauberpunkte
- Mana

Magie ist sehr tückisch und funktioniert nie so, wie man es sich wünscht. Immer wieder treten unvorhergesehene Probleme oder Effekte auf. Außerdem ist sie gefährlich in der Anwendung und schadet der Psyche. Das können auch die besten Magier nicht verhindern. Deshalb verlassen sich die meisten Zauberer nicht nur auf ihre Magie.

```admonish quote title='Von der großen Abenteurerin Elara Sturmreiter'
Nichts ist problematischer als in einem Abenteuer zu stehen und wehrlos zu sein.
```

## Kompetenzen

Folgende Magie-[Kompetenzen](/grundregelwerk/kompetenzen/index.md) stehen euch zur Verfügung:

- **Magietheorie:** 1 Magiekathegorie und 5 Zauberpunkte
- **Magiebegabung:** 5 Zauberpunkte und 3 Mana

```admonish info
Um Zaubern zu können benötigt ihr mindestens zwei Kompetenzen; einmal Magietheorie und einmal Magiebegabung.
```

## Aspekte

Mit einem Aspekt kann man einen Zauber für 2 Mana weniger wirken. So können Zauber auch ohne Manakosten gewirkt werden.

## Ressourcen

Mit Ressourcen kann man Zauberpunkte kaufen, der Umrechnungsfaktor ist 1:1.
