<!-- markdownlint-disable no-inline-html -->
# Datenschutzhinweise

## Einleitung

Wir habe uns bewusst gegen den Einsatz von Tracking-Cookies, Analyse-Werkzeugen oder andere direkte Einbindungen fremder Inhalte entschieden. Betreiber von verlinkten Webseiten, bekommen erst Ihre Informationen, wenn Sie den Link anklicken. Dieses vorgehen ist nicht dem »Mainstream« entsprichend und hat Nachteile für das Ranking bei Google und Co., aber wir ihretwillen gerne in Kauf. Dies handhaben wir deshalb so, um die [informationelle Selbstbestimmung](https://de.wikipedia.org/wiki/Informationelle_Selbstbestimmung) zu fördern bzw. Ihre IT-Sicherheit nicht unnötig zu gefährden.

Die verwendeten Begriffe sind nicht geschlechtsspezifisch.

## Name und Kontaktdaten des für die Verarbeitung Verantwortlichen

<p class="bot">
kcuB leinaD<br/>
<span style="font-size: 10px;">neinareM nov gozreH</span><br/>
2 .rtsnehcriK<br/>
nralsE 39629
</p>

**E-Mail-Adresse:** <span class="bot">retsnom.leipsnellor@retsambew</span><br/>
**Telefon:** <span class="bot">77736587 651 94+</span>

## Ansprechpartner für datenschutzrechtliche Fragen

Bei Fragen zum Datenschutz stehen wir Ihnen unter [datenschutz@rollenspiel.monster](mailto:datenschutz@rollenspiel.monster) <a href="https://rollenspiel.monster/.well-known/openpgpkey/hu/i7jib9hs56n983nznn7ohu6yz49drzkb?l=datenschutz"><i class="fa fa-gnupg" aria-hidden="true"></i>-Schlüssel</a> oder unter der oben angegebenen postalischen Anschrift zur Verfügung.

## Bereitstellung des Onlineangebotes und Webhosting

### Erhebung von Zugriffsdaten und Logfiles

Wenn Sie unsere Webseite besuchen, übermittelt Ihr Browser systembedingt gewisse Informationen an den Server, die Sie wiederum identifizierbar machen könnten. So übermitteln Sie bzw. Ihr Webbrowser z. B. Informationen wie die von Ihrem Provider zugewiesene IP-Adresse.
Dieses systembedingte Verhalten Ihres Browsers können Sie grundsätzlich nicht verhindern und es bleibt Ihnen deshalb auch nichts anderes übrig, als darauf zu vertrauen, dass wir mit den übermittelten Informationen sensibel umgehe. Weil uns – auch wenn es ein wenig abgedroschen klingen mag – der Schutz Ihrer Daten tatsächlich am Herzen liegt, werden wir die übermittelten Informationen bestmöglich Ihre Interessen vertretend handhaben. Aus diesem Grund erheben, verarbeiten oder nutzen wir die übermittelten Informationen nicht zu Analyse-, Werbezwecken oder Ähnlichem. Vielmehr speichern wir keine Informationen über Sie – der Webserver auf Basis von nginx ist so konfiguriert, dass er keine Logfiles schreibt:

```text
## LOGS ##
access_log off;
error_log off;
```

### Einsatz von Cookies

Cookies sind Textdateien, die Daten von besuchten Websites oder Domains enthalten und von einem Browser auf dem Computer des Benutzers gespeichert werden. Ein Cookie dient in erster Linie dazu, die Informationen über einen Benutzer während oder nach seinem Besuch innerhalb eines Onlineangebotes zu speichern. Zu den gespeicherten Angaben können z.B. die Spracheinstellungen auf einer Webseite, der Loginstatus, ein Warenkorb oder die Stelle, an der ein Video geschaut wurde, gehören. Zu dem Begriff der Cookies zählen wir ferner andere Technologien, die die gleichen Funktionen wie Cookies erfüllen (z.B., wenn Angaben der Nutzer anhand pseudonymer Onlinekennzeichnungen gespeichert werden, auch als "Nutzer-IDs" bezeichnet)

Die durch die Cookies verarbeiteten Daten sind für die Bereitstellung unserer Services und somit zur Wahrung unserer berechtigten Interessen sowie der Dritter nach Art. 6 Abs. 1 S. 1 lit. f DSGVO erforderlich. Bei den gesetzten Cookies, welche z.B. für den Login notwendig sind, handelt es sich um persistente Cookies, die allerdings nicht mit Tracking-Cookies von Drittanbietern verwechselt werden dürfen, die einen Nutzer seitenübergreifend über das Internet verfolgen.

**Hinweis**: Die meisten Browser akzeptieren Cookies automatisch. Sie können Ihren Browser jedoch so konfigurieren, dass keine Cookies auf Ihrem Computer gespeichert werden oder stets ein Hinweis erscheint, bevor ein neuer Cookie angelegt wird. Die vollständige Deaktivierung von Cookies kann jedoch dazu führen, dass Sie nicht alle Funktionen unserer Webseite nutzen können.

## Kontaktaufnahme

Bei der Kontaktaufnahme mit uns (z.B. per Kontaktformular, E-Mail, Telefon oder via soziale Medien) werden die Angaben der anfragenden Personen verarbeitet, soweit dies zur Beantwortung der Kontaktanfragen und etwaiger angefragter Maßnahmen erforderlich ist. Dabei ist die Angabe einer gültigen E-Mail-Adresse erforderlich, damit ich weiß, von wem die Anfrage stammt und um diese beantworten zu können. Weitere Angaben können freiwillig getätigt werden.

## Präsenzen in sozialen Netzwerken (Social Media)

Wir unterhalten Onlinepräsenzen innerhalb sozialer Netzwerke und verarbeiten in diesem Rahmen Daten der Nutzer, um mit den dort aktiven Nutzern zu kommunizieren oder um Informationen über uns anzubieten.

Wir weisen darauf hin, dass dabei Daten der Nutzer außerhalb des Raumes der Europäischen Union verarbeitet werden können. Hierdurch können sich für die Nutzer Risiken ergeben, weil so z.B. die Durchsetzung der Rechte der Nutzer erschwert werden könnte.

## Löschung von Daten

Die von uns verarbeiteten Daten werden nach Maßgabe der gesetzlichen Vorgaben gelöscht, sobald deren zur Verarbeitung erlaubten Einwilligungen widerrufen werden oder sonstige Erlaubnisse entfallen (z.B., wenn der Zweck der Verarbeitung dieser Daten entfallen ist oder sie für den Zweck nicht erforderlich sind).

Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung auf diese Zwecke beschränkt. D.h., die Daten werden gesperrt und nicht für andere Zwecke verarbeitet. Das gilt z.B. für Daten, die aus handels- oder steuerrechtlichen Gründen aufbewahrt werden müssen oder deren Speicherung zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person erforderlich ist.

Weitere Hinweise zu der Löschung von personenbezogenen Daten können ferner im Rahmen der einzelnen Datenschutzhinweise dieser Datenschutzerklärung erfolgen.

## Änderung und Aktualisierung der Datenschutzerklärung

Durch die Weiterentwicklung meiner Webseite und Angebote darüber oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern. Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf der Webseite unter dem Link von Ihnen abgerufen und ausgedruckt werden.

## Rechte der betroffenen Personen

Ihnen stehen als Betroffene nach der DSGVO verschiedene Rechte zu:

- **Gemäß Art.15 DSGVO** Auskunft über Ihre von mir verarbeiteten personenbezogenen Daten zu verlangen. Insbesondere können Sie Auskunft über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft ihrer Daten, sofern diese nicht bei mir erhoben wurden sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftigen Informationen zu deren Einzelheiten verlangen
- **Gemäß Art.16 DSGVO** unverzüglich die Berichtigung oder Vervollständigung Ihrer bei mir gespeicherten personenbezogenen Daten zu verlangen
- **Gemäß Art.17 DSGVO** die Löschung Ihrer bei mir gespeicherten personenbezogenen Daten zu verlangen, soweit nicht die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist
- **Gemäß Art.18 DSGVO** die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen, soweit die Richtigkeit der Daten von Ihnen bestritten wird, die Verarbeitung unrechtmäßig ist, Sie aber deren Löschung ablehnen, ich die Daten nicht mehr benötige, Sie jedoch diese zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen oder Sie gemäß Art.21 DSGVO Widerspruch gegen die Verarbeitung eingelegt haben
- **Gemäß Art.20 DSGVO** Ihre personenbezogenen Daten, die Sie mir bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen
- **Gemäß Art.7 Abs.3 DSGVO** Ihre einmal erteilte Einwilligung jederzeit gegenüber mir zu widerrufen. Dies hat zur Folge, dass ich die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen darf
- **Gemäß Art.77 DSGVO** sich bei einer Aufsichtsbehörde zu beschweren. In der Regel können Sie sich hierfür an die Aufsichtsbehörde Ihres üblichen Aufenthaltsortes oder Arbeitsplatzes wenden

## Externe Zahlungsdienstleister

Über Zahlungsdienstleister können Besucher dieser Webseite den Betreiber finanziell unterstützen. Es gelten die Datenschutzbestimmungen der jeweiligen Anbieter:

- Liberapay: [Datenschutz - Liberapay](https://liberapay.com/about/privacy)
- Banküberweisung: bei einer Überweisung wird in der Regel Vor- und Zuname, sowie Verwendungszweck, Buchungsdatum, IBAN, BIC und Betrag übermittelt.

## Adressverarbeitung

Alle die auf dieser Webseite angegebenen Kontaktinformationen von mir inklusive etwaiger Fotos dienen ausdrücklich nur zu Informationszwecken bzw. zur Kontaktaufnahme. Sie dürfen insbesondere nicht für die Zusendung von Werbung, Spam und ähnliches genutzt werden. Einer werblichen Nutzung dieser Daten wird deshalb hiermit widersprochen. Sollten diese Informationen dennoch zu den vorstehend genannten Zwecken genutzt werden, behalte ich mir etwaige rechtliche Schritte vor.
