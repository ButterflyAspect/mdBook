<!-- markdownlint-disable no-inline-html -->
# Einleitung

Willkommen in der Welt der Rollenspiele! Bevor wir uns ins Abenteuer stürzen, werfen wir einen kurzen Blick darauf, was Rollenspiele überhaupt sind.

## Was ist ein Rollenspiel?

Ein Rollenspiel ist ein interaktives Spiel, bei dem die Spieler in die Rolle eines fiktiven Charakters schlüpft und dessen Handlungen und Entscheidungen in einer imaginären Welt beeinflusst. Diese Charaktere können eine Vielzahl von Persönlichkeiten, Fähigkeiten und Hintergründen haben, und die Spieler haben die Freiheit, ihre Entwicklung während des Spiels zu steuern.

Das Besondere an Rollenspielen ist, dass man in eine vollständig fiktive Welt eintauchen und Geschichten erleben kann, die jenseits der Grenzen der realen Welt liegen. Dabei geht’s nicht nur darum, Ziele zu erreichen, sondern auch darum, die eigenen Charaktere zu entdecken, zu erforschen und zu entwickeln.

## Moirenico

ist ein innovatives, weltfreies Regelwerk, das den Spielern größtmögliche Freiheiten bei der Charaktererstellung und der Entwicklung ihrer Geschichten ermöglicht. Das Regelwerk legt den Fokus auf ein Schicksalspunkte-System. Damit können Spieler nicht nur schlechte Ergebnisse retten, sondern auch kreativ mit Charakterproblemen umgehen.
Eine weitere wichtige Komponente sind die Aspekte. Sie bringen Schwierigkeiten und Lösungen für die Charaktere. Damit wird das Spiel bereichert und die Charaktere können ihre Geschichte besser entwickeln.

Moirenico ist anders als andere Regelwerke, weil es so flexibel und offen ist. Es gibt keine festen Regeln, sondern die Spieler können mit ihren Schicksalspunkten selbst bestimmen, was passiert. Dadurch wird die Geschichte immer wieder neu und die Spieler können die Welt und ihre Charaktere aktiv mitgestalten.

Das Ergebnis ist ein spannendes und einzigartiges Spielerlebnis, das die Vorstellungskraft der Spieler herausfordert und ihnen ermöglicht, ihre Charaktere und Geschichten auf eine ganz neue Art und Weise zu entwickeln. Das Regelwerk legt den Schwerpunkt auf Kreativität, Zusammenarbeit und die Möglichkeit, unvorhergesehene Wendungen und Überraschungen zu erleben.
Eine der wichtigsten Regeln des Spiels ist Regel Null: **Hör auf die Spielleitung.**
Es ist zwar gut, wenn du die Spielleitung auf mögliche Probleme aufmerksam machst, aber niemand möchte wertvolle Spielzeit mit einer langwierigen Debatte verbringen. Während einer Sitzung ist es am besten, die Spielleitung die Dinge entscheiden zu lassen und ihrem Urteil zu vertrauen und den Dingen ihren Lauf zu lassen. So hat jeder Spaß.
Als Spielleitung solltest du aber vorsichtig mit dieser Regel umgehen, denn wenn du alle Entscheidungen, die den Spielspaß beeinträchtigen, mit Regel Null zu rechtfertigen willst, werden dir schnell die Spieler ausgehen. Denkt immer daran, dass das Ziel des Spiels darin besteht, dass alle eine schöne Zeit haben.
Eine klare und offene Kommunikation ist wichtig, um eine Spielgruppe am Laufen zu halten. Aber man muss auch wissen, dass man nicht immer den richtigen Zeitpunkt dafür hat, wenn es mitten im Geschehen ist.

## Aufteilung

Moirenico besteht aus verschiedenen Bereichen, die den Spielen zusätzliche Optionen und Möglichkeiten bieten. Das Grundregelwerk ist dabei der Kern des Systems und kann auch eigenständig genutzt werden. Es bietet eine solide Grundlage für das Spiel und ermöglicht den Spielen, sofort einzusteigen und ihre eigenen Geschichten zu entwickeln.
Das Magieregelwerk ist eine optionale Ergänzung zum Grundregelwerk und ermöglicht den Spielen, magische Fähigkeiten und Kräfte in ihre Charaktere einzubinden. Es enthält Regeln und Mechaniken, um verschiedene Arten von Magie darzustellen, sei es elementare Magie, Beschwörungen, Illusionen oder andere Formen sowie magische Gegenstände.

Das Konstruktionsregelwerk ist ein weiterer optionaler Zusatz und ermöglicht den Spielen, komplexe Objekte, Gebäude oder alchemistische Mixturen zu erschaffen. Es bietet Regeln und Richtlinien für das Entwerfen und Konstruieren von Gegenständen und Strukturen innerhalb des Spiels.

Diese Zusätze zum Grundregelwerk sind nach Belieben nutzbar. Die Spieler können sich entscheiden, nur das Grundregelwerk zu verwenden oder zusätzlich das Magieregelwerk und das Konstruktionsregelwerk einzuführen. So können sie das Spiel weiter anpassen und erweitern. Das ermöglicht eine flexible Anpassung des Regelwerks an die individuellen Vorlieben und Bedürfnisse der Spieler und fördert die Vielseitigkeit und Variation in den Spielsitzungen.

## Begriffserklärung

### Spielsitzung

Eine Spielsitzung ist ein Spieltag, an dem ihr euch trefft und zusammenspielt.

### Szenarios

Ein Szenario könnt ihr euch wie ein Kapitel in einem Buch vorstellen, das mehrere Spielsitzungen beinhaltet.

## Lizenz

Elemente dieses Regelwerks beruhen auf [Fate Creative Commons CC BY](https://www.faterpg.com/licensing/licensing-fate-cc-by/).

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://moireni.co">Moirenico</a> by <span property="cc:attributionName">Daniel Buck</span> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0</a></p>
