<!-- markdownlint-disable ol-prefix -->
# Moirenico

[![Codeberg CI](https://ci.codeberg.org/api/badges/Moirenico/mdBook/status.svg)](https://ci.codeberg.org/Moirenico/mdBook)
![Framework](https://img.shields.io/badge/Framework-mdBook-pink)
![Lizenz](https://img.shields.io/badge/Lizenz-GNU%20FDL%201.3-orange)

Dieses Repository enthält das Regelwerk für das Moirenico Pen-and-Paper-Rollenspielsystem im mdBook-Format.

## Inhaltsverzeichnis

- [Moirenico](#moirenico)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Über Moirenico](#über-moirenico)
  - [Installation](#installation)
  - [Beitragende](#beitragende)
  - [Kontakt](#kontakt)
  - [Lizenz](#lizenz)

## Über Moirenico

Das Moirenico beherbergt das Regelwerk für das Moirenico-Pen-and-Paper-Rollenspielsystem. Moirenico zielt darauf ab, den Spielern größtmögliche Freiheit bei der Charaktererschaffung und -gestaltung zu bieten und die Regeln so einfach wie möglich zu halten. Das Regelwerk enthält umfassende Informationen zu den Spielmechaniken, Charakterentwicklung, Fähigkeiten, Kampfsystem und vielem mehr.

## Installation

Um das Regelwerk lokal anzuzeigen, führen Sie bitte die folgenden Schritte aus:

1. Stellen Sie sicher, dass Sie eine aktuelle Version von mdBook auf Ihrem System installiert haben. Informationen zur Installation von mdBook finden Sie in der offiziellen mdBook-Dokumentation.

2. Klonen Sie das Repository mit Git:

    ```bash
    git clone https://codeberg.org/Moirenico/mdBook.git
    ```

3. Wechseln Sie in das Verzeichnis des mdBook-Projekts:

    ```bash
    cd mdBook
    ```

4. Führen Sie den Befehl `mdbook serve` aus, um den Entwicklungsserver zu starten.

5. Öffnen Sie Ihren Webbrowser und navigieren Sie zur Adresse `http://localhost:3000`, um das Regelwerk anzuzeigen.

## Beitragende

Beiträge zum Moirenico sind willkommen! Wenn Sie Verbesserungen am Regelwerk vornehmen möchten, führen Sie bitte die folgenden Schritte aus:

1. Forken Sie das Repository.
2. Erstellen Sie einen neuen Branch für Ihre Änderungen.
3. Führen Sie Ihre Änderungen durch und veröffentlichen Sie diese in Ihrem Fork.
4. Erstellen Sie einen Pull-Request, um Ihre Änderungen in das Hauptrepository einzubringen.

Wir schätzen Ihre Beiträge und werden sie überprüfen und zusammenführen, um das Moirenico-Rollenspielsystem kontinuierlich zu verbessern.

## Kontakt

Für weitere Informationen oder Fragen besuchen Sie bitte den Matrix-Chatraum [moirenico:rollenspiel.chat](https://matrix.to/#/#moirenico:rollenspiel.chat).

## Lizenz

Das Regelwerk des Moirenicos steht unter der CC BY-SA 4.0 License. Weitere Informationen finden Sie in der [Lizenzdatei](LICENSE).
