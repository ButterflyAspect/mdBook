---
ref: "main"
labels:
  - "Status: to triage"
---

<!--- Texte die mit <!- -> ummantelt sind, sind Beschreibungen und werden später nicht im Issue erscheinen. -->

**Um welches Regelwerk handelt es sich?**

- [ ] Grundregelwerk
- [ ] Magieregelwerk
- [ ] Konstruktionsregelwerk
- [ ] Stuntbeispiele

**Anliegen**

<!--- Beschreibe dein Anliegen -->
