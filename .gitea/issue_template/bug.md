---
name: "\U0001F41B Fehler"
about: "Ihr habt einen Fehler oder unstimmigkeiten entdeckt?"
title: ""
ref: "main"
labels:
  - "Kind: bug"
  - "Status: to triage"
---

<!--- Texte die mit <!- -> ummantelt sind, sind Beschreibungen und werden später nicht im Issue erscheinen. -->

**Um welches Regelwerk handelt es sich?**

- [ ] Grundregelwerk
- [ ] Magieregelwerk
- [ ] Konstruktionsregelwerk
- [ ] Material

**Fehlerbeschreibung**

<!--- Beschreibe um welchen Fehler es sich handelt -->

**Lösungsvorschlag**

<!--- Hier ist platz falls du einen Lösungsvorschlag hast -->

**Link zur Datei**

<!--- Bitte füge den Link zur Datei hinzu wo der Fehler zu finden ist -->
