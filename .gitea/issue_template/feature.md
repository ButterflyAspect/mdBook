---
name: "\U0001f4a1 Verbesserung"
about: "Ihr habt eine Idee um das Regelwerk zu erweiter und/oder zu verbessern?"
title: ""
ref: "main"
labels:
  - "Kind: feature"
  - "Status: to triage"
---

<!--- Texte die mit <!- -> ummantelt sind, sind Beschreibungen und werden später nicht im Issue erscheinen. -->

**Um welches Regelwerk handelt es sich?**

- [ ] Grundregelwerk
- [ ] Magieregelwerk
- [ ] Konstruktionsregelwerk
- [ ] Material

**Die Idee**

<!--- Beschreibe deine Idee -->
