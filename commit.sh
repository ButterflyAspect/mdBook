#!/bin/bash

# Function for displaying help information
help() {
    echo "Usage: $0 [OPTIONS]"
    echo
    echo "Options:"
    echo "  --merge         Merge the 'develop' branch into 'master' and apply a version bump in the commit message."
    echo "  --commit        Run the commit workflow to create a new commit."
    echo "  -h, --help      Display this help message."
    echo
    echo "If no options are provided, the script defaults to the commit workflow."
}

# Function for merging develop into master and applying version bump
perform_merge() {
    git checkout develop
    git pull origin develop

    git checkout master
    git pull origin master

    # Apply version bump after successful merge
    version_bump=$(select_version_bump)
    
    # Construct the merge commit message with the version bump
    merge_message="Merge 'develop' - Version bump: $version_bump"

    git merge --no-ff -m "$merge_message" develop

    if [[ $? -ne 0 ]]; then
        echo "Merge conflict detected. Please resolve conflicts and try again."
        exit 1
    else
        echo "Merge completed successfully with version bump: $version_bump."
    fi
    git checkout develop
}

# Function to select version bump type
select_version_bump() {
    bumps=("major" "minor" "patch")
    PS3="Please choose a version bump type (1-${#bumps[@]}): "
    select bump in "${bumps[@]}"; do
        case $bump in
            "major") echo "[major]"; break ;;
            "minor") echo "[minor]"; break ;;
            "patch") echo "[patch]"; break ;;
            *) printf "Invalid selection. Choose a number between 1 and ${#bumps[@]}.\n" ;;
        esac
    done
}

# Function to check for uncommitted or untracked changes
check_for_changes() {
    if [[ -z $(git diff --name-only) && -z $(git ls-files --others --exclude-standard) ]]; then
        printf "No changes to commit. Please ensure there are changes before proceeding.\n"
        exit 1
    fi
}

# Function to stage all changes
stage_changes() {
    printf "There are unstaged or untracked changes.\n"
    read -p "Do you want to stage all changes automatically? (y/N): " stage_all

    if [[ "$stage_all" == "y" || "$stage_all" == "Y" ]]; then
        git add .
        printf "All changes have been staged.\n"
    elif [[ -z $(git diff --cached --name-only) ]]; then
        printf "No staged changes present. Commit aborted.\n"
        exit 1
    fi
}

# Function to create the commit
create_commit() {
    # Choose commit type
    printf "Select a commit type:\n"
    commit_type=$(select_commit_type)

    # Choose scope
    printf "Select a scope:\n"
    scope=$(select_scope)

    # Enter commit description
    printf "Enter a description for the commit:\n"
    read -r description

    # Format commit message
    commit_message="$commit_type($scope): $description"

    # Create the commit
    git commit -m "$commit_message"
    printf "Commit created with message: $commit_message\n"
}

# Function to select commit type
select_commit_type() {
    options=("Added - for new features" "Changed - for changes to existing functionality" "Removed - for removed features" "Fixed - for bug fixes" "Deprecated - for soon-to-be removed features" "Security - for security issues")
    PS3="Choose a commit type (1-${#options[@]}): "
    select opt in "${options[@]}"; do
        case $opt in
            "Added - for new features") echo "Added"; break ;;
            "Changed - for changes to existing functionality") echo "Changed"; break ;;
            "Removed - for removed features") echo "Removed"; break ;;
            "Fixed - for bug fixes") echo "Fixed"; break ;;
            "Deprecated - for soon-to-be removed features") echo "Deprecated"; break ;;
            "Security - for security issues") echo "Security"; break ;;
            *) printf "Invalid selection. Choose a number between 1 and ${#options[@]}.\n" ;;
        esac
    done
}

# Function to select scope
select_scope() {
    scopes=("Grundregelwerk" "Magieregelwerk" "Konstruktionsregelwerk" "Material" "Infrastruktur")
    PS3="Choose a commit type (1-${#scopes[@]}): "
    select scope in "${scopes[@]}"; do
        case $scope in
            "Grundregelwerk") echo "grw"; break ;;
            "Magieregelwerk") echo "mrw"; break ;;
            "Konstruktionsregelwerk") echo "krw"; break ;;
            "Material") echo "mat"; break ;;
            "Infrastruktur") echo "infra"; break ;;
            *) printf "Invalid selection. Choose a number between 1 and ${#scopes[@]}.\n" ;;
        esac
    done
}

# Main function to run the commit workflow
main() {
    check_for_changes
    stage_changes
    create_commit
}

# Default values for options
merge_flag=false
commit_flag=false

# Parse command-line options manually
for arg in "$@"; do
    case $arg in
        --merge)
            merge_flag=true
            ;;
        --commit)
            commit_flag=true
            ;;
        -h|--help)
            help
            exit 0
            ;;
        *)
            echo "Invalid option: $arg"
            help
            exit 1
            ;;
    esac
done

# Execute functions based on the flags set
if $merge_flag; then
    perform_merge
elif $commit_flag || [[ $# -eq 0 ]]; then
    # Run main if --commit is specified or if no options were given
    main
else
    echo "No valid option provided."
    help
fi
